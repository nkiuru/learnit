//
//  courses.swift
//  pathfinderTests
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
@testable import pathfinder
import XCTest
import CoreData
import Foundation

class pathfinderCoursesTests: XCTestCase {
    
    override func setUp() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Courses")
        
        // Create Batch Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try PersistenceService.context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    override func tearDown() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Courses")
        
        // Create Batch Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try PersistenceService.context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
        
    }
    
    func testCreateCourse() {
        PersistenceService.context.performAndWait {
            do {
                if let course = try? Courses.createCourse(id: 0, context: PersistenceService.context) {
                    if let courseItem = course {
                        courseItem.descr = "test"
                        courseItem.duration = 12.5
                        courseItem.ects = Int64(15)
                        courseItem.id = Int64(0)
                        courseItem.language = "test"
                        courseItem.level = Int64(1)
                        courseItem.price = 192.5
                        courseItem.providerCourseID = ""
                        courseItem.providerID = 0
                        courseItem.title = "test"
                        courseItem.www = "test"
                        courseItem.favourited = 1 // TODO
                    }
                }
            }
        }
        if let course = getCourse(id: 0){
            XCTAssert(course.id == 0)
        } else {
            XCTAssert(false)
        }
    }
    
    func testCreateMultipleCourses() {
        PersistenceService.context.performAndWait {
            do {
                if let course = try? Courses.createCourse(id: 0, context: PersistenceService.context) {
                    if let courseItem = course {
                        courseItem.descr = "test"
                        courseItem.duration = 12.5
                        courseItem.ects = Int64(15)
                        courseItem.id = Int64(0)
                        courseItem.language = "test"
                        courseItem.level = Int64(1)
                        courseItem.price = 192.5
                        courseItem.providerCourseID = ""
                        courseItem.providerID = 0
                        courseItem.title = "test"
                        courseItem.www = "test"
                        courseItem.favourited = 1 // TODO
                    }
                }
                if let course = try? Courses.createCourse(id: -1, context: PersistenceService.context) {
                    if let courseItem = course {
                        courseItem.descr = "test"
                        courseItem.duration = 12.5
                        courseItem.ects = Int64(15)
                        courseItem.id = Int64(-1)
                        courseItem.language = "test"
                        courseItem.level = Int64(1)
                        courseItem.price = 192.5
                        courseItem.providerCourseID = ""
                        courseItem.providerID = 0
                        courseItem.title = "test"
                        courseItem.www = "test"
                        courseItem.favourited = 1 // TODO
                    }
                }
            }
        }
        if let course = getCourse(id: -1){
            XCTAssert(course.id == -1)
        } else {
            XCTAssert(false)
        }
    }
    
    func testCreateDuplicate() {
        PersistenceService.context.performAndWait {
            do {
                if let course = try? Courses.createCourse(id: 0, context: PersistenceService.context) {
                    if let courseItem = course {
                        courseItem.descr = "test"
                        courseItem.duration = 12.5
                        courseItem.ects = Int64(15)
                        courseItem.id = Int64(0)
                        courseItem.language = "test"
                        courseItem.level = Int64(1)
                        courseItem.price = 192.5
                        courseItem.providerCourseID = ""
                        courseItem.providerID = 0
                        courseItem.title = "test"
                        courseItem.www = "test"
                        courseItem.favourited = 1 // TODO
                    }
                }
                if let course = try? Courses.createCourse(id: 0, context: PersistenceService.context) {
                    if let courseItem = course {
                        courseItem.descr = "test"
                        courseItem.duration = 12.5
                        courseItem.ects = Int64(15)
                        courseItem.id = Int64(0)
                        courseItem.language = "test"
                        courseItem.level = Int64(1)
                        courseItem.price = 192.5
                        courseItem.providerCourseID = ""
                        courseItem.providerID = 0
                        courseItem.title = "test"
                        courseItem.www = "test"
                        courseItem.favourited = 1 // TODO
                    }
                }
            }}
        if let courses = getCourses(){
            XCTAssert(courses.count == 1)
        } else {
            XCTAssert(false)
        }
    }
}
