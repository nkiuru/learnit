//
//  pathfinderObjectiveCClassTests.swift
//  pathfinderTests
//
//  Created by Samuli Virtanen on 02/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

@testable import pathfinder
import XCTest
import Foundation

class pathfinderObjectiveCClassTests: XCTestCase {
    
    func testObjectiveCClassWithParameters() {
        
        let objCTestClass = PFMyInfoObject()
        
        objCTestClass.courseCountEnrolled = 15
        objCTestClass.courseCountFinished = 20
        objCTestClass.courseECTSGathered = 25
        objCTestClass.courseECTSEnrolled = 30
            
        XCTAssert(objCTestClass.courseCountEnrolled == 15)
        XCTAssert(objCTestClass.courseCountFinished == 20)
        XCTAssert(objCTestClass.courseECTSGathered == 25)
        XCTAssert(objCTestClass.courseECTSEnrolled == 30)
        
    }
    
    func testObjectiveCClassWithoutParameters() {
        
        let objCTestClass = PFMyInfoObject()
        
        XCTAssert(objCTestClass.courseCountEnrolled == 0)
        XCTAssert(objCTestClass.courseCountFinished == 0)
        XCTAssert(objCTestClass.courseECTSGathered == 0)
        XCTAssert(objCTestClass.courseECTSEnrolled == 0)
        
    }
    
}
