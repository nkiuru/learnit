

import XCTest
@testable import pathfinder

class pathfinderJSONRequestTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Test if JSONRequest (backend-api) call works and gets response with SUCCESS: TRUE
    func testRequestSuccessResponse() {
        var testPassed = false
        let exp = self.expectation(description: "myExpectation")
        JSONRequest(operation: "categories", parameters: nil) { (res) in
            if res.success {
                testPassed = true
                exp.fulfill()
            }
        }
        waitForExpectations(timeout: 5) { (error) in
            XCTAssert(testPassed)
        }
    }
    
    // Test if JSONRequest (backend-api) call works and returns error message with invalid login query
    func testRequestErrorMessage() {
        var testPassed = false
        let exp = self.expectation(description: "myExpectation")
        JSONRequest(operation: "login", parameters: nil) { (res) in
            if res.success {
                testPassed = false
            } else {
                if res.error != "" {
                    testPassed = true
                    exp.fulfill()
                }
            }
        }
        waitForExpectations(timeout: 5) { (error) in
            XCTAssert(testPassed)
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testEnrollAndDeEnroll() {
        // Login --> Enroll to course 10 --> De-Enroll from course 10 --> Success.
        var testPassed = false
        let exp = self.expectation(description: "myExpectation")
        JSONRequest(operation: "login", parameters: ["username": "testi", "password": "1234"]) { (res) in
            print("LOGIN SUCCESS = \(res.success)")
            if res.success {
                print(PFLoginGetSessionToken)
                JSONRequest(operation: "enroll", parameters: ["course_id": "10"]) { (res) in
                    if res.success {
                        JSONRequest(operation: "enroll", parameters: ["course_id": "10", "remove": "true"], parseFunction: { (res) in
                            testPassed = true
                            exp.fulfill()
                        })
                    }
                }
            } else {
                print (res.error)
            }
        }
        waitForExpectations(timeout: 5) { (error) in
            XCTAssert(testPassed)
        }
    }
        
}
