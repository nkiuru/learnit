//
//  pathfinderTests.swift
//  pathfinderTests
//
//  Created by iosdev on 16/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import XCTest
@testable import pathfinder

class pathfinderAPIDataTests: XCTestCase {
    
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testAPIDataLoadCoursesAndStoreToCoreData() {
        let exp = self.expectation(description: "myExpectation")
        var testSucceeded = false
        var errorMessage = ""
        APIData(load: .Courses) { (dataLoadSucceeded, dataLoadErrorMessage) in
            errorMessage = dataLoadErrorMessage
            if dataLoadSucceeded {
                testSucceeded = true
                exp.fulfill()
            }
        }
        waitForExpectations(timeout: 5) { (error) in
            XCTAssert(testSucceeded, errorMessage)
        }
    }

       
}
