//
//  pathfinderUITests.swift
//  pathfinderUITests
//
//  Created by iosdev on 16/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import XCTest

class pathfinderUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
        app.launchArguments.append("--uitesting")
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    // Tests if allows login w/o credentials
    func testLogin() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        app.launch()
        
        let isLogin = app.otherElements["loginView"].exists
        XCTAssert(isLogin)
        
        let acceptableUsername = "testi"
        let acceptablePassword = "1234"
        
        let usernameField = app.otherElements.textFields["usernameField"]
        let passwordField = app.otherElements.secureTextFields["passwordField"]
        let loginBtn = app.otherElements.buttons["loginButton"]

        XCTAssertTrue(usernameField.exists)
        XCTAssertTrue(passwordField.exists)
        XCTAssertTrue(loginBtn.exists)
        
        usernameField.tap()
        usernameField.typeText(acceptableUsername)
        passwordField.tap()
        passwordField.typeText(acceptablePassword)
        
        loginBtn.tap()
        
        sleep(4)
        
        XCTAssertTrue(app.otherElements["homeView"].exists)
        app.terminate()
    }

    func testLoginErrorWithFalseCredentials() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        app.launch()
        
        let isLogin = app.otherElements["loginView"].exists
        XCTAssert(isLogin)
        
        let notAcceptableUsername = "testi_THIS:Username-does-not-exists"
        let notAcceptablePassword = "1234-password IS WRONG!!!"
        
        let usernameField = app.otherElements.textFields["usernameField"]
        let passwordField = app.otherElements.secureTextFields["passwordField"]
        let loginBtn = app.otherElements.buttons["loginButton"]
        
        XCTAssertTrue(usernameField.exists)
        XCTAssertTrue(passwordField.exists)
        XCTAssertTrue(loginBtn.exists)
        
        usernameField.tap()
        usernameField.typeText(notAcceptableUsername)
        passwordField.tap()
        passwordField.typeText(notAcceptablePassword)
        
        loginBtn.tap()
        
        sleep(4)
        
        XCTAssertFalse(app.otherElements["homeView"].exists)
        app.terminate()
    }
    
    func testCreateAccount() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        app.launch()
        
        let isLogin = app.otherElements["loginView"].exists
        XCTAssert(isLogin)

        let btnSignup = app.otherElements.buttons["signUpButton"]
        XCTAssertTrue(btnSignup.exists)
        btnSignup.tap()

        sleep(1)
        
        let randomNumber1 = Int.random(in: 0 ... 99999999)
        let randomNumber2 = Int.random(in: 0 ... 99999999)
        let randomUser = "UITEST" + String(randomNumber1) + String(randomNumber2)
        
        let textUsername = app.otherElements.textFields["textUsername"]
        let textEmail = app.otherElements.textFields["textEmail"]
        let textFullname = app.otherElements.textFields["textFullname"]
        let textPassword = app.otherElements.secureTextFields["textPassword"]
        let textPasswordAgain = app.otherElements.secureTextFields["textPasswordAgain"]
        let btnSignupSubmit = app.otherElements.buttons["btnSignUp"]
        
        XCTAssertTrue(textUsername.exists)
        XCTAssertTrue(textFullname.exists)
        XCTAssertTrue(textEmail.exists)
        XCTAssertTrue(textPassword.exists)
        XCTAssertTrue(textPasswordAgain.exists)
        XCTAssertTrue(btnSignupSubmit.exists)
        
        textUsername.tap()
        textUsername.typeText(randomUser)
        textEmail.tap()
        textEmail.typeText(randomUser + "@UITest.com")
        textFullname.tap()
        textFullname.typeText(randomUser)
        textPassword.tap()
        textPassword.typeText(randomUser)
        textPasswordAgain.tap()
        textPasswordAgain.typeText(randomUser)
        
        btnSignupSubmit.tap()
        
        sleep(1)
        
        XCTAssert(app.otherElements["onboardingView"].exists)
        
        app.terminate()
    }

    
}
