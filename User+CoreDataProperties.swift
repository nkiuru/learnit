//
//  User+CoreDataProperties.swift
//  pathfinder
//
//  Created by iosdev on 29/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var email: String?
    @NSManaged public var fullname: String?
    @NSManaged public var id: Int64
    @NSManaged public var username: String?
    @NSManaged public var session: Session?

}
