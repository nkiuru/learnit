//
//  User+CoreDataClass.swift
//  pathfinder
//
//  Created by iosdev on 29/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {
    class func createUser(id: Int64, context: NSManagedObjectContext) throws -> User? {
        let request: NSFetchRequest<User> = User.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingUsers = try context.fetch(request)
            if matchingUsers.count == 1 {
                return nil
            } else if matchingUsers.count == 0 {
                return User(context: context)
            } else {
                print("Duplicate user")
                return nil
            }
        } catch {
            throw error
        }
    }
}
