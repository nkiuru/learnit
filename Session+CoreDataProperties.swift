//
//  Session+CoreDataProperties.swift
//  pathfinder
//
//  Created by iosdev on 29/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension Session {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Session> {
        return NSFetchRequest<Session>(entityName: "Session")
    }

    @NSManaged public var token: String?
    @NSManaged public var id: Int64
    @NSManaged public var user: User?

}
