//
//  StudypathSuggester.swift
//  pathfinder
//
//  Created by Niklas Kiuru on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

/*
     _____ __            __                  __  __   _____                             __
    / ___// /___  ______/ /_  ______  ____ _/ /_/ /_ / ___/__  ______ _____ ____  _____/ /____  _____
    \__ \/ __/ / / / __  / / / / __ \/ __ `/ __/ __ \\__ \/ / / / __ `/ __ `/ _ \/ ___/ __/ _ \/ ___/
   ___/ / /_/ /_/ / /_/ / /_/ / /_/ / /_/ / /_/ / / /__/ / /_/ / /_/ / /_/ /  __(__  ) /_/  __/ /
  /____/\__/\__,_/\__,_/\__, / .___/\__,_/\__/_/ /_/____/\__,_/\__, /\__, /\___/____/\__/\___/_/  .swift
                       /____/_/                               /____//____/
 
 
 StudypathSuggester ALGORITHM & LOGIC
 
 */

import Foundation


class StudypathSuggester {
    
    private var categoryDictionary: [Categories: Int] = [:]
    private let parentCategories = PFCategoriesFetchMain()
    
    // Takes an array of 5 - 10 subcategories & user skill level
    // Based on them, will return 3 most fitting categories of IT to study
    func suggestStudypath(chosenCategories: [Categories], skillLevel: Int) -> [StudyPaths]?{
        guard let categories = parentCategories else {
            return nil
        }
        for cat in categories {
            categoryDictionary[cat] = 0
        }
        
        let amountOfEachSubcategory = 5
        let percentSuggestion = (100 / amountOfEachSubcategory)
        
        for chosenCat in chosenCategories {
            let category = getParent(id: chosenCat.parent)
            if let parent = category {
                categoryDictionary[parent] = (categoryDictionary[parent] ?? 0) + percentSuggestion
            } else {
                print("[StudypathSuggester:suggestStudypath] parent category not found")
            }
        }
        
        let sortedCategories = categoryDictionary.sorted(by: { $0.value > $1.value })
        var keys: [Categories] = []
        
        for item in sortedCategories {
            keys.append(item.key)
            print((item.key.category ?? "No name")  + ":" + String(item.value) + "%")
        }
        
        // TODO: Figure out way to take into account if user selects same amount of subcategories in many main categories
        
        // Lazy way to get top 3 categories
        let topCategories: [Categories] = Array(keys.prefix(3))
        return self.getStudyPath(categories: topCategories, skillLevel: skillLevel)
    }
    
    // Takes the most interesting categories to the user & returns a list of study paths
    // TODO: Update function to return study path objects instead of categories
    private func getStudyPath(categories: [Categories], skillLevel: Int) -> [StudyPaths]?{
        let paths = PFCategoriesFetchStudypaths(skillLevel: skillLevel)
        var topPaths: [StudyPaths] = []
        if let paths = paths {
            topPaths = paths.filter {path in
                var matches = true
                for category in categories {
                    matches = category.id != path.category
                }
                return matches
            }
            return topPaths
        } else {
            print("[StudypathSuggester: getStudyPath] No studypaths found")
            return nil
        }
    }
    
    private func getParent(id: Int64) -> Categories?{
        guard let categories = parentCategories else {
            return nil
        }
        let parent = (categories.filter {category in
            return category.id == id
        })
        return (parent.count > 0) ? parent[0] : nil
    }
}
