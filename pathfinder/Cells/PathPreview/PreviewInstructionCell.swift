//
//  PreviewInstructionCell.swift
//  pathfinder
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class PreviewInstructionCell: UITableViewCell {
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subTitleLabel: UILabel!
  
  func setTheme() {
    titleLabel.textColor = UIColor.PF.Title
    subTitleLabel.textColor = UIColor.PF.Text
    backgroundColor = UIColor.PF.PageBackground
  }
  
}
