//
//  PathCell.swift
//  pathfinder
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class PathCell: UITableViewCell {
  
  @IBOutlet weak var pathNameLable: UILabel!
  @IBOutlet weak var numOfCoursesLabel: UILabel!
  @IBOutlet weak var lenghtLabel: UIView!
  
  func setData(courses: [Courses], name: String) {
    numOfCoursesLabel.text = "\(courses.count) courses"
    pathNameLable.text = name
    var lenght: NSDecimalNumber = 0
    for course in courses {
      if let duration = course.duration {
        lenght = lenght.adding(duration)
      }
    }
    
    let lengthText = lenghtLabel.subviews[0] as! UILabel
    lengthText.text = "\(lenght)h"
    
  }
  
  func setTheme() {
    backgroundColor = UIColor.PF.PageBackground
    pathNameLable.textColor = UIColor.PF.Title
    numOfCoursesLabel.textColor = UIColor.PF.Text
    
    lenghtLabel.backgroundColor = UIColor.PF.Label
    lenghtLabel.layer.cornerRadius = 3
    let lenghtText = lenghtLabel.subviews[0] as! UILabel
    lenghtText.textColor = UIColor.PF.LabelText
  }

}
