//
//  FavouriteCell.swift
//  pathfinder
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

struct DummyFavourite {
  let descr: String?
  let duration: NSDecimalNumber?
  let ects: Int64
  let id: Int64
  let language: String?
  let level: Int64
  let price: NSDecimalNumber?
  let providerCourseID: String?
  let providerID: Int64
  let title: String?
  let www: String?
  let favourited: Int64
  let enrolled: Int64
  let finished: Int64
}

class FavouriteCell: UITableViewCell {

  @IBOutlet weak var cellRootView: UIView!
  @IBOutlet weak var courseNameLabel: UILabel!
  @IBOutlet weak var courseProviderLabel: UILabel!
  @IBOutlet weak var levelLabel: UIView!
  @IBOutlet weak var priceLabel: UIView!
  @IBOutlet weak var statusLabel: UIView!
  
  
  func setData(data: Courses) {
    courseNameLabel.text = data.title
    if let provider = getProvider(id: data.providerID) {
        courseProviderLabel.text = provider.name
    }
    if let levelText = levelLabel.subviews[0] as? UILabel {
      levelText.text = setLevel(data.level)
    }
    
    if let priceText = priceLabel.subviews[0] as? UILabel {
      guard let price = data.price else { fatalError("Favourite course error: price") }
     priceText.text = setPrice(price)
    }
    
    setupStatusLabel(data)
    
  }
  
  private func setPrice(_ price: NSDecimalNumber) -> String {
    if price == 0 {
      return NSLocalizedString("Free", comment: "").uppercased()
    } else {
      let formatter = NumberFormatter()
      formatter.numberStyle = .currency
      formatter.locale = Locale(identifier: "de_DE")
      guard let formattedPrice = formatter.string(from: price) else { fatalError("Course info error: number formatter error from price") }
      return formattedPrice
    }
  }
  
  private func setLevel(_ lvlInt: Int64) -> String {
    switch lvlInt {
    case 0:
        return NSLocalizedString("Beginner", comment: "")
    case 1:
        return NSLocalizedString("Intermediate", comment: "")
    case 2:
        return NSLocalizedString("Advanced", comment: "")
    default:
      return "incorrect data"
    }
  }
  
  private func setupStatusLabel(_ data: Courses) {
    
    guard let statusText = statusLabel.subviews[0] as? UILabel else { fatalError("Error") }
    statusLabel.isHidden = false

    if data.finished == 1 {
      statusLabel.layer.backgroundColor = UIColor.PF.MainGreen.cgColor
      statusText.text = NSLocalizedString("Finished", comment: "").uppercased()
      return
    }
    
    if data.enrolled == 1 {
      statusLabel.layer.backgroundColor = UIColor.PF.EnrolledYellow.cgColor
      statusText.text = NSLocalizedString("Enrolled", comment: "").uppercased()
      return
    }
    
    else {
      statusLabel.isHidden = true
    }
    
  }
  
  func setTheme() {
    cellRootView.backgroundColor = UIColor.PF.PageBackground
    
    courseNameLabel.textColor = UIColor.PF.Title
    courseProviderLabel.textColor = UIColor.PF.Text
    
    levelLabel.layer.cornerRadius = 3
    levelLabel.layer.backgroundColor = UIColor.PF.Label.cgColor
    let levelText = levelLabel.subviews[0] as! UILabel
    levelText.textColor = UIColor.PF.LabelText
    
    priceLabel.layer.cornerRadius = 3
    priceLabel.layer.backgroundColor = UIColor.PF.Label.cgColor
    let priceText = priceLabel.subviews[0] as! UILabel
    priceText.textColor = UIColor.PF.LabelText
    
    statusLabel.layer.cornerRadius = 3
    statusLabel.layer.backgroundColor = UIColor.PF.Label.cgColor
    
  }
  
}
