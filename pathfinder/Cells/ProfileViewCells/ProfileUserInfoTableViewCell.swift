//
//  ProfileUserInfoTableViewCell.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class ProfileUserInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFullName: UILabel!
    @IBOutlet weak var labelEmailAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(fullName: String, email: String) {
        labelFullName?.text = fullName
        labelEmailAddress?.text = email
    }
    
    func setTheme() {
        self.backgroundColor = UIColor.PF.PageBackground
        labelFullName?.textColor = UIColor.PF.Title
        labelEmailAddress?.textColor = UIColor.PF.Text
    }
    
}
