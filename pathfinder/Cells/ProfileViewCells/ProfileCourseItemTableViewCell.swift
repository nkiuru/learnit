//
//  ProfileCourseItemTableViewCell.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 05/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class ProfileCourseItemTableViewCell: UITableViewCell {

    @IBOutlet weak var labelCourseTypeTitle: UILabel!
    @IBOutlet weak var labelCourseTypeCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func setData(title: String, count: String) {
        self.labelCourseTypeCount?.text = count
        self.labelCourseTypeTitle?.text = title
    }
    
    func setTheme() {
        self.backgroundColor = UIColor.PF.PageBackground
        labelCourseTypeTitle.textColor = UIColor.PF.Title
        labelCourseTypeCount.textColor = UIColor.PF.Text
    }

}
