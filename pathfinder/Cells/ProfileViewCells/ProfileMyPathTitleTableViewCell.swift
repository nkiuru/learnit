//
//  ProfileMyPathTitleTableViewCell.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 05/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class ProfileMyPathTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setTheme() {
        self.backgroundColor = UIColor.PF.PageBackground
        labelTitle.textColor = UIColor.PF.Title
    }

}
