//
//  ProfileMyPathItemTableViewCell.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 05/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class ProfileMyPathItemTableViewCell: UITableViewCell {

    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var labelItemTitle: UILabel!
    @IBOutlet weak var labelItemStatus: UILabel!
    @IBOutlet weak var labelItemLevel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(data: Courses) {
        labelItemTitle.text = data.title
        if let levelText = labelItemLevel {
            levelText.text = setLevel(data.level)
        }
        setupStatusLabel(data)
        if data.finished == 1 {
            imageIcon.image = UIImage(named: "Finished 20x20")
        } else {
            imageIcon.image = UIImage(named: "Enrolled 20x20")
        }
        
    }
    
    private func setLevel(_ lvlInt: Int64) -> String {
        switch lvlInt {
        case 0:
            return NSLocalizedString("Beginner", comment: "")
        case 1:
            return NSLocalizedString("Intermediate", comment: "")
        case 2:
            return NSLocalizedString("Advanced", comment: "")
        default:
            return "incorrect data"
        }
    }
    
    private func setupStatusLabel(_ data: Courses) {
        
        guard let statusText = labelItemStatus else { fatalError("Error") }
        labelItemStatus.isHidden = false
        
        if data.finished == 1 {
            statusText.text = NSLocalizedString("Finished", comment: "")
            return
        }
        
        if data.enrolled == 1 {
            statusText.text = NSLocalizedString("Enrolled", comment: "")
            return
        }
            
        else {
            labelItemStatus.isHidden = true
        }
        
    }
    
    func setTheme() {
        self.backgroundColor = UIColor.PF.PageBackground
        self.labelItemTitle.textColor = UIColor.PF.Title
        self.labelItemStatus.textColor = UIColor.PF.Text
        self.labelItemLevel.textColor = UIColor.PF.Title
    }

}
