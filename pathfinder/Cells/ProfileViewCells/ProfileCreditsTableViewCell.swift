//
//  ProfileCreditsTableViewCell.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 05/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class ProfileCreditsTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var labelCreditsMIN: UILabel!
    @IBOutlet weak var labelCreditsMAX: UILabel!
    @IBOutlet weak var labelCreditsATM: UILabel!
    
    var heightSizeIsChanged = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(enrolledCredits: Int32, finishedCredits: Int32) {
        let progress: Float = Float(Float(finishedCredits) / Float(enrolledCredits))
        self.progressView.progress = progress
        
        self.labelCreditsMIN.text = "0"
        self.labelCreditsMAX.text = String(enrolledCredits)
        self.labelCreditsATM.text = String(finishedCredits)
    }

    func setTheme() {
        self.backgroundColor = UIColor.PF.PageBackground
        self.labelTitle.textColor = UIColor.PF.Title
        self.labelDescription.textColor = UIColor.PF.Text
        self.labelCreditsMIN.textColor = UIColor.PF.Title
        self.labelCreditsMAX.textColor = UIColor.PF.Title
        self.labelCreditsATM.textColor = UIColor.PF.Title
        self.progressView.progressTintColor = UIColor.PF.MainGreen
        self.progressView.trackTintColor = UIColor.PF.Card
        if !heightSizeIsChanged {
            self.progressView.transform = self.progressView.transform.scaledBy(x: 1, y: 1.5) // More height to ProgressView
            heightSizeIsChanged = true
        }
    }
    
}
