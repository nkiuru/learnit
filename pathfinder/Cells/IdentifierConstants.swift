//
//  IdentifierConstants.swift
//  pathfinder
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import Foundation

// Cell identifier constants
struct IDENTIFIER {
  static let HEADER = "headerCell"
  static let COURSE_INFO = "courseInfoCell"
  static let DESCRIPTION = "descriptionCell"
  static let PREREQUISITES = "prerequisitsCell"
  static let DISCOVER_ITEM = "discoverCell"
  static let FAVOURITE_ITEM = "favouriteCell"
  static let COURSES_ITEM = "courseCell"
}
