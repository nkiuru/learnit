//
//  CourseDescriptionCell.swift
//  pathfinder
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class CourseDescriptionCell: UITableViewCell {
  
  @IBOutlet weak var rootView: UIView!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var headerLabel: UILabel!
  
  func setTheme() {
    rootView.backgroundColor = UIColor.PF.PageBackground
    descriptionLabel.textColor = UIColor.PF.Text
    headerLabel.textColor = UIColor.PF.Title
  }
  
  func setData(data: Courses) {
    descriptionLabel.text = data.descr
    headerLabel.text = NSLocalizedString("Description", comment: "")
  }

}
