//
//  CoursePrerequisitesCell.swift
//  pathfinder
//
//  Created by iosdev on 28/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class CoursePrerequisitesCell: UITableViewCell {

  @IBOutlet weak var rootView: UIView!
  @IBOutlet weak var collectionView: UICollectionView!
  
  override func awakeFromNib() {
    collectionView.delegate = self
    collectionView.dataSource = self
    
    // Setup cell spacing
    let layout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 10)
    layout.itemSize = CGSize(width: 180, height: 83)
//    layout.minimumLineSpacing = 0
//    layout.minimumInteritemSpacing = 0
    layout.scrollDirection = .horizontal
    collectionView.collectionViewLayout = layout
  }
  
  func setTheme() {
    collectionView.backgroundColor = UIColor.PF.PageBackground
  }
  
}

// MARK: UICollectionView delegates
extension CoursePrerequisitesCell: UICollectionViewDelegate, UICollectionViewDataSource {
  
  // Number of cells
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return 5
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvCell", for: indexPath as IndexPath) as! SinglePrerequisiteCell
    cell.setTheme()
    return cell
  }
  
}
