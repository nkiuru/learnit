//
//  SinglePrerequisiteCell.swift
//  pathfinder
//
//  Created by iosdev on 28/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class SinglePrerequisiteCell: UICollectionViewCell {
  
  @IBOutlet weak var statusUIImage: UIImageView!
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var courseNameLabel: UILabel!
  @IBOutlet weak var providerLabel: UILabel!
  
  func setTheme() {
    backgroundColor = UIColor.PF.Card
    layer.cornerRadius = 4
    
    statusLabel.textColor = UIColor.PF.Title
    courseNameLabel.textColor = UIColor.PF.Text
    providerLabel.textColor = UIColor.PF.Text
  }
  
}
