//
//  CourseCategoryCell.swift
//  pathfinder
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import SafariServices

class CourseInfoCell: UITableViewCell {
  
  @IBOutlet weak var rootView: UIView!
  @IBOutlet weak var courseNameLabel: UILabel!
  @IBOutlet weak var providerLabel: UILabel!
  @IBOutlet weak var infoBoxView: UIView!
  @IBOutlet weak var creditsLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var levelLabel: UILabel!
  @IBOutlet weak var lenghtLabel: UILabel!
  @IBOutlet weak var enrollButton: PFPrimaryButton!
  @IBOutlet weak var exitCourseButton: PFSecondaryButton!
  @IBOutlet weak var finishCourseButton: PFPrimaryButton!
  @IBOutlet weak var courseLinkButton: PFSecondaryButton!
  
  var course: Courses?
  
  
  func setTheme() {
    rootView.backgroundColor = UIColor.PF.PageBackground
    courseNameLabel.textColor = UIColor.PF.Title
    providerLabel.textColor = UIColor.PF.Text
    
    infoBoxView.backgroundColor = UIColor.PF.Card
    infoBoxView.layer.cornerRadius = 4
    for case let label as UILabel in infoBoxView.subviews {
      label.textColor = UIColor.PF.Title
    }
    
  }
  
  func setData(data: Courses, _ isPreview: Bool) {
    course = data
    print("Course data for course info screen: \(String(describing: course))")
    courseNameLabel.text = data.title
    
    if let provider = getProvider(id: data.providerID) {
      providerLabel.text = provider.name
    }
    
    levelLabel.text = setLevel(lvlInt: data.level)
    
    guard let duration = data.duration else { fatalError("Course data error: duration") }
    lenghtLabel.text = setLength(duration)
    
    guard let price = data.price else { fatalError("Course data error: price") }
    priceLabel.text = setPrice(price)
    
    if data.ects == 0 {
      creditsLabel.text = "0"
    } else {
      creditsLabel.text = "+\(data.ects)"
    }
    
    // Hide enroll button if user is looking at course preview
    if isPreview {
      enrollButton.isEnabled = false
      enrollButton.isHidden = true
      enrollButton.heightAnchor.constraint(equalToConstant: 0).isActive = true
    }
    
    checkCourseStatus()
    
  }
  
  private func checkCourseStatus() {
    if course?.enrolled == 0 && course?.finished == 0 {
      exitCourseButton.isHidden = true
      finishCourseButton.isHidden = true
      return
    }

    else if course?.enrolled == 1 && course?.finished == 1 {
      enrollButton.isHidden = true
      finishCourseButton.isHidden = true
      exitCourseButton.isHidden = true
      return
    }
    
    else if course?.enrolled == 1 {
      enrollButton.isHidden = true
    }
    
  }
  
  private func setPrice(_ price: NSDecimalNumber) -> String {
    if price == 0 {
      return NSLocalizedString("Free", comment: "")
    } else {
      let formatter = NumberFormatter()
      formatter.numberStyle = .currency
      formatter.locale = Locale(identifier: "de_DE")
      guard let formattedPrice = formatter.string(from: price) else { fatalError("Course info error: number formatter error from price") }
      return formattedPrice
    }
  }
  
  private func setLength(_ lenght: NSDecimalNumber) -> String {
    if lenght == 0 {
      return "-"
    } else {
      return "\(lenght)h"
    }
  }
  
  private func setLevel(lvlInt: Int64) -> String {
    let pre = Locale.preferredLanguages[0]
    print(pre)
    switch lvlInt {
    case 0:
      return NSLocalizedString("Beginner", comment: "")
    case 1:
      return NSLocalizedString("Intermediate", comment: "")
    case 2:
      return NSLocalizedString("Advanced", comment: "")
    default:
      return "incorrect data"
    }
  }
  
}
