//
//  CourseInfoHeaderCell.swift
//  pathfinder
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class CourseInfoHeaderCell: UITableViewCell {
 
  @IBOutlet weak var headerLabel: UILabel!
  
  func setHeader(header: String) {
    self.contentView.backgroundColor = UIColor.PF.PageBackground
    headerLabel.text = header
    headerLabel.textColor = UIColor.PF.Title
  }

}
