//
//  CollectionViewCell.swift
//  pathfinder
//
//  Created by Sara Suviranta on 05/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class OnboardingCell: UICollectionViewCell {
  @IBOutlet weak var obLabel: UILabel!
  
  func setTheme() {
    self.layer.cornerRadius = 4
    backgroundColor = UIColor.PF.Card
    obLabel.textColor = UIColor.PF.MainGreen
  }
}
