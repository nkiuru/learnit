//
//  SummaryCell.swift
//  pathfinder
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var enrolled: UILabel!
    @IBOutlet weak var finished: UILabel!
    @IBOutlet weak var credits: UILabel!
    @IBOutlet weak var enrolledNumber: UILabel!
    @IBOutlet weak var finishedNumber: UILabel!
    @IBOutlet weak var creditsDescription: UILabel!
    @IBOutlet weak var creditsProgress: UIProgressView!
    @IBOutlet weak var labelCreditsATM: UILabel!
    @IBOutlet weak var labelCreditsMIN: UILabel!
    @IBOutlet weak var labelCreditsMAX: UILabel!
    @IBOutlet weak var summaryCardBG: UIView!
    
    var heightSizeIsChanged = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(enrolledCredits: Int32, finishedCredits: Int32, courseCountEnrolled: Int32, courseCountFinished: Int32) {
        let progress: Float = Float(Float(finishedCredits) / Float(enrolledCredits))
        self.creditsProgress.progress = progress
        
        self.enrolledNumber.text = String(courseCountEnrolled)
        self.finishedNumber.text = String(courseCountFinished)
        self.labelCreditsMIN.text = "0"
        self.labelCreditsMAX.text = String(enrolledCredits)
        self.labelCreditsATM.text = String(finishedCredits)
    }
    
    func setTheme() {
        self.backgroundColor = UIColor.PF.PageBackground
        self.summaryCardBG.backgroundColor = UIColor.PF.Card
        self.title.textColor = UIColor.PF.Title
        self.enrolled.textColor = UIColor.PF.Title
        self.finished.textColor = UIColor.PF.Title
        self.credits.textColor = UIColor.PF.Title
        self.enrolledNumber.textColor = UIColor.PF.Title
        self.finishedNumber.textColor = UIColor.PF.Title
        self.creditsDescription.textColor = UIColor.PF.Text
        self.labelCreditsMIN.textColor = UIColor.PF.Title
        self.labelCreditsMAX.textColor = UIColor.PF.Title
        self.labelCreditsATM.textColor = UIColor.PF.Title
        self.creditsProgress.progressTintColor = UIColor.PF.MainGreen
        self.creditsProgress.trackTintColor = UIColor.PF.PageBackground
        if !heightSizeIsChanged {
            self.creditsProgress.transform = self.creditsProgress.transform.scaledBy(x: 1, y: 1.5) // More height to ProgressView
            heightSizeIsChanged = true
        }
    }

}
