//
//  HeaderCell.swift
//  pathfinder
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class HomeHeaderCell: UITableViewCell {
  
  @IBOutlet weak var headerLable: UILabel!
  
  func setHeader(value: String) {
    headerLable.text = value
  }
  
  func setTheme() {
    headerLable.textColor = UIColor.PF.Title
    backgroundColor = UIColor.PF.PageBackground
  }
}
