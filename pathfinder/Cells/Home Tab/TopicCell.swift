//
//  TopicCell.swift
//  pathfinder
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class TopicCell: UICollectionViewCell {
  
  @IBOutlet weak var buttonTitleLabel: UILabel!
  
  func setTheme() {
    self.layer.cornerRadius = 4
    backgroundColor = UIColor.PF.Card
    buttonTitleLabel.textColor = UIColor.PF.MainGreen
  }
  
  func setTitle(title: String) {
    buttonTitleLabel.text = title
  }
}
