//
//  TopicsCell.swift
//  pathfinder
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

protocol TopicCellSelectionDelegate {
    func didSelect(id: Int)
}

class TopicsCell: UITableViewCell {
  
  @IBOutlet weak var headerLabel: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
    var delegate: TopicCellSelectionDelegate?

    var topics: [Categories]? = nil
    override func awakeFromNib() {
        super.awakeFromNib()
      
      collectionView.delegate = self
      collectionView.dataSource = self
      
      let layout = UICollectionViewFlowLayout()
      layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 10)
      layout.itemSize = CGSize(width: 160, height: 120)
      layout.scrollDirection = .horizontal
      collectionView.collectionViewLayout = layout
      
    }
    
  func setTheme() {
    backgroundColor = UIColor.PF.PageBackground
    headerLabel.textColor = UIColor.PF.Title
  }

}

extension TopicsCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return topics?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topicCell", for: indexPath as IndexPath) as! TopicCell
    cell.setTheme()
    cell.setTitle(title: topics?[indexPath.row].category ?? "Sample Topic")
    return cell
  }
  
  // TODO: Navigate to search and pass category
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("select")
        delegate?.didSelect(id: indexPath.row)
    }
}
