//
//  JourneyItemCell.swift
//  pathfinder
//
//  Created by iosdev on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class JourneyItemCell: UITableViewCell {
  
  @IBOutlet weak var enrolledIcon: UIImageView!
  @IBOutlet weak var courseHeader: UILabel!
  @IBOutlet weak var courseStatus: UILabel!
  @IBOutlet weak var courseDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(data: Courses) {
        
        courseHeader.text = data.title
        if let levelText = courseDescription {
            levelText.text = setLevel(data.level)
        }
        setupStatusLabel(data)
        if data.finished == 1 {
            enrolledIcon.image = UIImage(named: "Finished 20x20")
        } else {
            enrolledIcon.image = UIImage(named: "Enrolled 20x20")
        }
        
    }
    
    private func setLevel(_ lvlInt: Int64) -> String {
        switch lvlInt {
        case 0:
            return NSLocalizedString("Beginner", comment: "")
        case 1:
            return NSLocalizedString("Intermediate", comment: "")
        case 2:
            return NSLocalizedString("Advanced", comment: "")
        default:
            return "incorrect data"
        }
    }
    
    private func setupStatusLabel(_ data: Courses) {
        
        guard let statusText = courseStatus else { fatalError("Error") }
        courseStatus.isHidden = false
        
        if data.finished == 1 {
            statusText.text = NSLocalizedString("Finished", comment: "")
            return
        }
        
        if data.enrolled == 1 {
            statusText.text = NSLocalizedString("Enrolled", comment: "")
            return
        }
            
        else {
            courseStatus.isHidden = true
        }
        
    }
  func setTheme() {
    self.backgroundColor = UIColor.PF.PageBackground
    self.courseHeader.textColor = UIColor.PF.Title
    self.courseStatus.textColor = UIColor.PF.Text
    self.courseDescription.textColor = UIColor.PF.Title
  }

}
