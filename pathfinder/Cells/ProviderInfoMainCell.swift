//
//  ProviderInfoMainCell.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class ProviderInfoMainCell: UITableViewCell {

    @IBOutlet weak var imageProviderLogo: UIImageView!
    @IBOutlet weak var labelTopCaption: UILabel!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var labelDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setTheme() {
        mainView.backgroundColor = UIColor.PF.PageBackground
        labelTopCaption.textColor = UIColor.PF.Title
        labelDescription.textColor = UIColor.PF.Text
    }
    
}
