//
//  PFTextField.swift
//  pathfinder
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class PFTextField: UITextField {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupTextFiled()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupTextFiled()
  }
  
  func setupTextFiled() {
    backgroundColor = UIColor.PF.SABBackground
    borderStyle = .none
    clipsToBounds = true
    layer.cornerRadius = 8
    textColor = .white
    keyboardAppearance = .dark
    setLeftPaddingPoints(12)
    setRightPaddingPoints(12)
  }
  
}
