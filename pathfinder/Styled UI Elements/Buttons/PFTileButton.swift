//
//  PFTileButton.swift
//  pathfinder
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class PFTileButton: UIButton {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupButton()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupButton()
  }
  
  func setupButton() {
    layer.backgroundColor = UIColor.PF.Card.cgColor
    layer.cornerRadius = 4
    setTitleColor(UIColor.PF.MainGreen, for: .normal)
  }
  
}
