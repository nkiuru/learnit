//
//  PFPrimaryButton.swift
//  pathfinder
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class PFPrimaryButton: UIButton {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupButton()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupButton()
  }
  
  func setupButton() {
    layer.cornerRadius = 8
    backgroundColor = UIColor.PF.MainGreen
    setTitleColor(UIColor.PF.MainBlue, for: .normal)
  }
  
}
