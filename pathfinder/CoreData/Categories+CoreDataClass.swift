//
//  Categories+CoreDataClass.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Categories)
public class Categories: NSManagedObject {
    class func createCategory(id: Int64, context: NSManagedObjectContext) throws -> Categories? {
        let request: NSFetchRequest<Categories> = Categories.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingCats = try context.fetch(request)
            if matchingCats.count == 1 {
                return matchingCats[0]
            } else if matchingCats.count == 0 {
                return Categories(context: context)
            } else {
                print("Duplicate categories")
                return nil
            }
        } catch {
            throw error
        }
    }
}
