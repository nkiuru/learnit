//
//  StudyPaths+CoreDataProperties.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension StudyPaths {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<StudyPaths> {
        return NSFetchRequest<StudyPaths>(entityName: "StudyPaths")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var category: Int64
    @NSManaged public var path: String?
    @NSManaged public var level: Int64
    @NSManaged public var descr: String?

}
