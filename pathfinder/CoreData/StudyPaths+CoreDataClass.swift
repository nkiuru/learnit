//
//  StudyPaths+CoreDataClass.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(StudyPaths)
public class StudyPaths: NSManagedObject {
    class func createStudyPath(id: Int64, context: NSManagedObjectContext) throws -> StudyPaths? {
        let request: NSFetchRequest<StudyPaths> = StudyPaths.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingSPs = try context.fetch(request)
            if matchingSPs.count == 1 {
                return nil
            } else if matchingSPs.count == 0 {
                return StudyPaths(context: context)
            } else {
                print("Duplicate matchingSPs")
                return nil
            }
        } catch {
            throw error
        }
    }
}
