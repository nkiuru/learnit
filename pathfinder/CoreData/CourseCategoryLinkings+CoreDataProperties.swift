//
//  CourseCategoryLinkings+CoreDataProperties.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension CourseCategoryLinkings {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CourseCategoryLinkings> {
        return NSFetchRequest<CourseCategoryLinkings>(entityName: "CourseCategoryLinkings")
    }

    @NSManaged public var category: Int64
    @NSManaged public var course: Int64
    @NSManaged public var id: Int64

}
