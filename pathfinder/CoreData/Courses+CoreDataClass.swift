//
//  Courses+CoreDataClass.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Courses)
public class Courses: NSManagedObject {
    class func createCourse(id: Int64, context: NSManagedObjectContext) throws -> Courses? {
        let request: NSFetchRequest<Courses> = Courses.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingCourses = try context.fetch(request)
            if matchingCourses.count == 1 {
                return matchingCourses[0]
            } else if matchingCourses.count == 0 {
                return Courses(context: context)
            } else {
                print("Duplicate courses")
                return nil
            }
        } catch {
            throw error
        }
    }
}
