	//
//  Languages+CoreDataProperties.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 02/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension Languages {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Languages> {
        return NSFetchRequest<Languages>(entityName: "Languages")
    }

    @NSManaged public var code: String?
    @NSManaged public var id: Int64
    @NSManaged public var language: String?

}
