//
//  Providers+CoreDataClass.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 30/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Providers)
public class Providers: NSManagedObject {
    class func createProvider(id: Int64, context: NSManagedObjectContext) throws -> Providers? {
        let request: NSFetchRequest<Providers> = Providers.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingProviders = try context.fetch(request)
            if matchingProviders.count == 1 {
                return matchingProviders[0]
            } else if matchingProviders.count == 0 {
                return Providers(context: context)
            } else {
                print("[Providers+CoreDataClass] ERROR: Duplicate providers")
                return nil
            }
        } catch {
            throw error
        }
    }
}
