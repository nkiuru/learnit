//
//  CourseCategoryLinkings+CoreDataClass.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(CourseCategoryLinkings)
public class CourseCategoryLinkings: NSManagedObject {
    class func createLink(id: Int64, context: NSManagedObjectContext) throws -> CourseCategoryLinkings? {
        let request: NSFetchRequest<CourseCategoryLinkings> = CourseCategoryLinkings.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingCCLs = try context.fetch(request)
            if matchingCCLs.count == 1 {
                return matchingCCLs[0]
            } else if matchingCCLs.count == 0 {
                return CourseCategoryLinkings(context: context)
            } else {
                print("Duplicate CCLs")
                return nil
            }
        } catch {
            throw error
        }
    }
}
