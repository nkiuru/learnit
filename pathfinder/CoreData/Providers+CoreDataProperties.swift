//
//  Providers+CoreDataProperties.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension Providers {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Providers> {
        return NSFetchRequest<Providers>(entityName: "Providers")
    }

    @NSManaged public var address: String?
    @NSManaged public var city: String?
    @NSManaged public var country: String?
    @NSManaged public var id: Int64
    @NSManaged public var logoURL: String?
    @NSManaged public var name: String?
    @NSManaged public var webpageURL: String?
    @NSManaged public var descr: String?

}
