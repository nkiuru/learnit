//
//  Courses+CoreDataProperties.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 03/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension Courses {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Courses> {
        return NSFetchRequest<Courses>(entityName: "Courses")
    }

    @NSManaged public var descr: String?
    @NSManaged public var duration: NSDecimalNumber?
    @NSManaged public var ects: Int64
    @NSManaged public var enrolled: Int64
    @NSManaged public var favourited: Int64
    @NSManaged public var finished: Int64
    @NSManaged public var id: Int64
    @NSManaged public var language: String?
    @NSManaged public var level: Int64
    @NSManaged public var price: NSDecimalNumber?
    @NSManaged public var providerCourseID: String?
    @NSManaged public var providerID: Int64
    @NSManaged public var title: String?
    @NSManaged public var www: String?
    @NSManaged public var categories: String?
    @NSManaged public var studypath: Int64
    @NSManaged public var finish_time: NSDate?
    @NSManaged public var enroll_time: NSDate?
    @NSManaged public var favourite_time: NSDate?

}
