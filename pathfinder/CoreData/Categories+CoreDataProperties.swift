//
//  Categories+CoreDataProperties.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData


extension Categories {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Categories> {
        return NSFetchRequest<Categories>(entityName: "Categories")
    }

    @NSManaged public var category: String?
    @NSManaged public var id: Int64
    @NSManaged public var level: Int64
    @NSManaged public var parent: Int64
    @NSManaged public var popular: Int64

}
