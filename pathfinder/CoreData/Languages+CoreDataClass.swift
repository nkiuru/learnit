//
//  Languages+CoreDataClass.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 02/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Languages)
public class Languages: NSManagedObject {
    class func createLanguage(id: Int64, context: NSManagedObjectContext) throws -> Languages? {
        let request: NSFetchRequest<Languages> = Languages.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingLanguages = try context.fetch(request)
            if matchingLanguages.count == 1 {
                return nil
            } else if matchingLanguages.count == 0 {
                return Languages(context: context)
            } else {
                print("Duplicate matchingLanguages")
                return nil
            }
        } catch {
            throw error
        }
    }
}
