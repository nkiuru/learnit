//
//  CourseInfoViewController.swift
//  pathfinder
//
//  Created by iosdev on 27/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import SafariServices

// indexPath.section constants
private struct SECTION {
  static let COURSE_INFO = 0
  //static let PREREQUISITES = 1
  static let DESCRIPTION = 1
}


class CourseInfoViewController: BaseViewController {
    
    var courseID: Int64 = 0
    var providerID: Int64 = 0
    
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var favouriteUIImage: UIBarButtonItem!
  var course: Courses? = nil
  var isPreview = false
  
  // If nil there is no section header in UITableView
  let sections: [String?] = [
    nil, // course info
    //NSLocalizedString("Prerequisites", comment: ""),
    nil //NSLocalizedString("Description", comment: ""),
  ]
  
  private var isFavourite = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = UIColor.PF.PageBackground
    
    if let course = course {
        print(course)
        self.courseID = course.id
        self.providerID = course.providerID
    }
    checkIsFavourite()
    checkIsPreview()
  }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "CourseInfoToProviderInfo" {
            guard let providerInfoViewController = segue.destination as? ProviderInfoViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            providerInfoViewController.providerIDSelected = self.providerID
        }
    }
  
  @IBAction func onFavouriteTap(_ sender: Any) {
    toggleFavourite()
  }
    
    @IBAction func onGestureRecognizerTap(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "CourseInfoToProviderInfo", sender: nil)
    }
    
    @IBAction func onLinkTap(_ sender: UIButton!) {
    if let course = course {
      guard let wwwString = course.www else { fatalError("Course info error: url") }
      guard let courseUrl = URL(string: wwwString) else { fatalError("URL error") }
      let safariVC = SFSafariViewController(url: courseUrl)
      present(safariVC, animated: true, completion: nil)
    }
  }
  
  @IBAction func onEnrollTap(_ sender: Any) {

    let alertController = UIAlertController(title: NSLocalizedString("Enroll to Course", comment: ""), message: NSLocalizedString("Do you want to Enroll to this Course?", comment: ""), preferredStyle: .alert)
    let actionLogoutButtonPress = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (action:UIAlertAction) in
        self.showSpinner(onView: self.view)
        print("[CourseInfoViewController] Enroll: User enrolled, trying to enroll to the course...")
        PFEnrollToCourse(courseID: self.courseID, parseFunction: { (success, error) in
            if (success) {
                print("[CourseInfoViewController] Enrolling to the Course SUCCEEDED.")
              DispatchQueue.main.async {
                print("Reloading course info row")
                self.course?.enrolled = 1
//                let indexPath = IndexPath(item: 0, section: 0)
                //self.tableView.reloadRows(at: [indexPath], with: .fade)
                self.tableView.reloadSections(IndexSet(integersIn: 0...1), with: .fade)

              }
            } else {
                print("[CourseInfoViewController] Enrolling to the Course FAILED.")
            }
            self.removeSpinner()
        })
    }
    let actionCancelButtonPress = UIAlertAction(title: NSLocalizedString("No(idont)", comment: ""), style: .cancel) { (action:UIAlertAction) in
        print("[CourseInfoViewController] Enroll: User cancelled.")
    }
    alertController.addAction(actionCancelButtonPress)
    alertController.addAction(actionLogoutButtonPress)
    self.present(alertController, animated: true, completion: nil)


  }
  
  @IBAction func onExitCourseTap(_ sender: Any) {

    print("Exit course tapped")
    let alertController = UIAlertController(title: NSLocalizedString("Do you want to exit this Course?", comment: ""), message: "", preferredStyle: .alert)
    let actionFinishCourseButtonPress = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (action:UIAlertAction) in
        self.showSpinner(onView: self.view)
        print("[CourseInfoViewController] Trying to exit the course...")
        PFEnrollToCourseCancelEnrollment(courseID: self.courseID, parseFunction: { (success, error) in
            if (success) {
                print("[CourseInfoViewController] Exiting the Course SUCCEEDED.")
                DispatchQueue.main.async {
                    print("Reloading course info row")
                    self.course?.enrolled = 0
                    self.course?.finished = 0
                    self.tableView.reloadSections(IndexSet(integersIn: 0...1), with: .fade)
                    
                }
            } else {
                print("[CourseInfoViewController] Exiting the Course FAILED.")
            }
            self.removeSpinner()
        })
    }
    
    let actionCancelButtonPress = UIAlertAction(title: NSLocalizedString("No(idont)", comment: ""), style: .cancel) { (action:UIAlertAction) in
        print("[CourseInfoViewController] Finish: User cancelled.")
    }
    alertController.addAction(actionCancelButtonPress)
    alertController.addAction(actionFinishCourseButtonPress)
    self.present(alertController, animated: true, completion: nil)
    
  }
  
  @IBAction func onFinishCourseTap(_ sender: Any) {
    print("Finish tapped!")
    let alertController = UIAlertController(title: "Finish course?", message: "", preferredStyle: .alert)
    let actionFinishCourseButtonPress = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .default) { (action:UIAlertAction) in
      self.showSpinner(onView: self.view)
      print("[CourseInfoViewController] Finish: User finished course, trying to finish the course...")
      PFFinishCourse(courseID: self.courseID, parseFunction: { (success, error) in
        if (success) {
          print("[CourseInfoViewController] Finishing the Course SUCCEEDED.")
          DispatchQueue.main.async {
            print("Reloading course info sections")
            self.course?.finished = 1
            //                let indexPath = IndexPath(item: 0, section: 0)
            //self.tableView.reloadRows(at: [indexPath], with: .fade)
            self.tableView.reloadSections(IndexSet(integersIn: 0...1), with: .fade)
          }
        } else {
          print("[CourseInfoViewController] Finishing the Course FAILED.")
        }
        self.removeSpinner()
      })
    }

    let actionCancelButtonPress = UIAlertAction(title: NSLocalizedString("No(idont)", comment: ""), style: .cancel) { (action:UIAlertAction) in
      print("[CourseInfoViewController] Finish: User cancelled.")
    }
    alertController.addAction(actionCancelButtonPress)
    alertController.addAction(actionFinishCourseButtonPress)
    self.present(alertController, animated: true, completion: nil)
    
  }
  
  private func toggleFavourite() {
    if !isFavourite {
      favouriteUIImage.image = UIImage(named: "course_favourite_selected")
    } else {
      favouriteUIImage.image = UIImage(named: "course_favourite_unselected")
    }
    isFavourite = !isFavourite
    print(isFavourite)
    let _ = updateCourse(id: course?.id ?? 0, key: "favourited", value: isFavourite ? 1 : 0)
    JSONRequest(operation: "favourite", parameters: ["course_id": String(courseID), "remove": (isFavourite == true ? "false" : "true")]) { (	response) in
        print("JSONReq: Favorite: SUCCESS=\(response.success) ERROR=\(response.error)")
        PFFavoriteListUpdated(courseIDThatHasChangedFavoriteStatus: self.courseID, newFavoriteStatus: self.isFavourite)
    }
  }
  
  private func checkIsFavourite() {
    guard let course = course else { fatalError("Course info error") }
    if course.favourited == 0 {
      isFavourite = false
      favouriteUIImage.image = UIImage(named: "course_favourite_unselected")
    } else if course.favourited == 1 {
      isFavourite = true
      favouriteUIImage.image = UIImage(named: "course_favourite_selected")
    }
  }
  
  private func checkIsPreview() {
    if isPreview {
      print("This is a course preview.")
      favouriteUIImage.isEnabled = false
      favouriteUIImage.tintColor = UIColor.clear
    }
  }
  
}

// MARK: UITableView delegates
extension CourseInfoViewController: UITableViewDelegate, UITableViewDataSource {
  
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return sections.count
  }
  
  // Header height
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0
  }
  

  
  // Return 1 because all sections have only one row.
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 1
  }
  
  
  // Setion cell and setup
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch indexPath.section {
    
    case SECTION.COURSE_INFO:
      let cell = tableView.dequeueReusableCell(withIdentifier: IDENTIFIER.COURSE_INFO) as! CourseInfoCell
      guard let courseData = course else { fatalError("Course data error") }
      cell.setData(data: courseData, isPreview)
      cell.setTheme()
      return cell
      
    case SECTION.DESCRIPTION:
      let cell = tableView.dequeueReusableCell(withIdentifier: IDENTIFIER.DESCRIPTION) as! CourseDescriptionCell
      guard let courseData = course else { fatalError("Course data error") }
      cell.setData(data: courseData)
      cell.setTheme()
      return cell
      
    default:
      let cell = UITableViewCell()
      return cell
    }
    
  }
  
}
