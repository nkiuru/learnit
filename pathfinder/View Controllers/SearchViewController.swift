//
//  DiscoverViewController.swift
//  pathfinder
//
//  Created by iosdev on 23/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
// ------------------------------------------------------------------------------------------------------------
//   This view controller:
//     1) Fetches course data from core data
//     2) Allows user to search for course by keyword
//     3) Displays all courses in a table view
// ------------------------------------------------------------------------------------------------------------

import UIKit
import CoreData

class SearchViewController: BaseViewController, NSFetchedResultsControllerDelegate, UISearchResultsUpdating {
    // TODO: Add segmented control for searching based on some property
    //private let refreshControl = UIRefreshControl()
    var fetchedResultsController: NSFetchedResultsController<Courses>?
    private var filteredCourses = [Courses]()
    private let searchController = UISearchController(searchResultsController: nil)
    var categoryIdForSearch: String? = nil
    
    private let COURSE_INFO_SEGUE_IDENTIFIER = "DiscoverToCourseInfo"
    
    @IBOutlet weak var discoverListTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup pull-to-refresh. Does not work with large titles when not using tableviewcontroller
        //        if #available(iOS 10.0, *) {
        //            discoverListTableView.refreshControl = refreshControl
        //        } else {
        //            discoverListTableView.addSubview(refreshControl)
        //        }
        //        refreshControl.addTarget(self, action: #selector(refreshCourseData(_:)), for: .valueChanged)
        discoverListTableView.delegate = self
        discoverListTableView.dataSource = self
        discoverListTableView.backgroundView = UIView()
        //fetchCourseData()
        let fetchRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        // Searching for courses based on category ID if given
        if let catId = categoryIdForSearch {
            // Regex for checking if csv string contains id
            let format = "categories MATCHES %@"
            let pattern = String(format:".*(\\b%@\\b).*", catId);
            fetchRequest.predicate = NSPredicate.init(format: format, pattern)
        }
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: PersistenceService.context, sectionNameKeyPath: "title", cacheName: nil)
        fetchedResultsController?.delegate = self as NSFetchedResultsControllerDelegate
        try? fetchedResultsController?.performFetch()
        configureSearchController()
        searchController.searchResultsUpdater = self
        discoverListTableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == COURSE_INFO_SEGUE_IDENTIFIER {
            let destVC = segue.destination as! CourseInfoViewController
            destVC.course = sender as? Courses
        }
        
    }
    
    //    @objc func refreshCourseData(_ sender: Any) {
    //        // Fetch Course Data
    //        fetchCourseData()
    //    }
    
//    private func fetchCourseData() {
//        // Fetch data from the server & display loading spinner if updating manually
//        APIData(load: .Courses) { (success, error) in
//            print("[DiscoverViewController] APIData[load: .Courses] Success=\(success) \(error)")
//            if success {
//                DispatchQueue.main.async {
//                    self.discoverListTableView.reloadData()
//                    //self.refreshControl.endRefreshing()
//                }
//            }
//        }
//    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        //print("changed content")
        discoverListTableView.reloadData()
    }
    
}

// MARK: UITableView delegates
extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive {
            return filteredCourses.count
        }
        
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive {
            return 1
        }
        return fetchedResultsController!.sections?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = discoverListTableView.dequeueReusableCell(withIdentifier: IDENTIFIER.DISCOVER_ITEM, for: indexPath) as? DiscoverItemCell else {
            fatalError(":(")
        }
        guard let course = searchController.isActive ? filteredCourses[indexPath.row] : self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Item not found")
        }
        
        cell.setTheme()
        cell.setData(data: course)
        
        return cell
    }
    
    // When row is selected perform a segue to course info
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let course = searchController.isActive ? filteredCourses[indexPath.row] : self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Item not found")
        }
        performSegue(withIdentifier: COURSE_INFO_SEGUE_IDENTIFIER, sender: course)
    }
    
}
// Extension for UI search bar
extension SearchViewController: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterSearchController(searchBar: searchBar, scope: searchBar.scopeButtonTitles![selectedScope])
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterSearchController(searchBar: searchBar, scope: scope)
    }
    
    func configureSearchController() {
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.barTintColor = UIColor.PF.MainBlue
        searchController.searchBar.tintColor = UIColor.PF.MainGreen
        definesPresentationContext = true
        searchController.searchBar.scopeButtonTitles = ["All", "Paid", "Free"]
        searchController.searchBar.delegate = self
        discoverListTableView.tableHeaderView = searchController.searchBar
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterSearchController(searchBar: UISearchBar, scope: String = "All") {
        let searchText = searchBar.text ?? ""
        let courses = fetchedResultsController?.fetchedObjects
        filteredCourses = ((courses?.filter { course in
            var doesCategoryMatch = (scope == "All")
            if (scope == "Paid") {
                let comp = course.price?.compare(0)
                doesCategoryMatch = comp == ComparisonResult.orderedDescending
            } else if(scope == "Free") {
                doesCategoryMatch = course.price?.isEqual(to: 0) ?? false
            }
            if searchBarIsEmpty() {
                return doesCategoryMatch
            } else {
                return doesCategoryMatch && course.title!.lowercased().contains(searchText.lowercased()) || searchText.lowercased().count == 0
            }
            })!)
        self.discoverListTableView.reloadData()
    }
}

