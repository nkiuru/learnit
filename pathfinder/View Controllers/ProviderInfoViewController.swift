//
//  ProviderInfoViewController.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import SafariServices

class ProviderInfoViewController: BaseViewController {

    @IBOutlet var baseView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var providerIDSelected: Int64 = 0
    
    var providerName = ""
    var providerDescription = ""
    var providerLogoURI = ""
    var providerWebsiteURI = ""
    var providerImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.PF.PageBackground
        if let providerInfo = getProvider(id: providerIDSelected) {
            providerName = providerInfo.name ?? ""
            providerDescription = providerInfo.descr ?? ""
            providerLogoURI = providerInfo.logoURL ?? ""
            providerWebsiteURI = providerInfo.webpageURL ?? ""
            guard let url = URL(string: providerLogoURI) else {
                print("providerLogoURI failed.")
                return
            }
            downloadImage(from: url)
        } else {
            print("[ProviderInfoViewController] Could not get provider info by ID \(providerIDSelected)")
        }
    }
    
    @IBAction func buttonGoToWebPageClick(_ sender: UIButton) {
        if self.providerWebsiteURI != "" {
            guard let providerURL = URL(string: self.providerWebsiteURI) else { fatalError("URL error") }
            let safariVC = SFSafariViewController(url: providerURL)
            present(safariVC, animated: true, completion: nil)
        } else {
            print("[ProviderInfoViewController] ERROR: No webpage URI available.")
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download finished")
            self.providerImage = UIImage(data: data)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
}



extension ProviderInfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // Header height
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    // Return 1 because all sections have only one row.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    // Setion cell and setup
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProviderInfoMainCell") as! ProviderInfoMainCell
        cell.labelDescription.text = self.providerDescription
        cell.labelTopCaption.text = self.providerName
        cell.imageProviderLogo.image = self.providerImage
        cell.setTheme()
        return cell
        
    }
    
}
