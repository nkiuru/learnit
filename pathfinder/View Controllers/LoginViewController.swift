//
//  LoginViewController.swift
//  pathfinder
//
//  Created by Sara Suviranta on 23/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController {
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var usernameField: PFTextField!
  @IBOutlet weak var passwordField: PFTextField!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    hideKeyboardGesture()
    
    //usernameField.text = "testi"
    //passwordField.text = "1234"
    scrollView.backgroundColor = UIColor.PF.PageBackground
    
    if let user = PFLoginFetchUserFromCD() {
      PFLoginSetStateLogin(user: user)
      gotoHomeNavigationSegue()
    }
  }
  
  func gotoHomeNavigationSegue() {
    setRootController()
    performSegue(withIdentifier: "toHome", sender: self)
  }
  
  @IBAction func login(_ sender: UIButton) {
    
    // TODO: PROMPT WHEN ERRORS WITH USERNAME/PASSWORD INPUT ! (fields empty etc.)
    
    guard let username = usernameField.text else {
      print("username not set.")
      return
    }
    
    guard let password = passwordField.text else {
      print("Password not set.")
      return
    }
    self.showSpinner(onView: self.view)
    JSONRequest(operation: "login", parameters: ["username": username, "password": password], setCustomTimeout: 7) { (response) in
      
      if response.success {
        // Login Succeeded!
        if PFLoginSaveUserToCD(jsonResponse: response).0{
          DispatchQueue.main.async {
            self.removeSpinner()
            self.gotoHomeNavigationSegue()
          }
        }
        // Set Login Status (TRUE):
        // Hide Login and Navigate --> Main View
      } else {
        
        // Login Failed :(
        
        DispatchQueue.main.async {
          self.removeSpinner()
          let alertController = UIAlertController(title: NSLocalizedString("Login Failed", comment: ""), message: response.error, preferredStyle: .alert)
          let actionOKButtonPress = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            // Action if needed.
          }
          alertController.addAction(actionOKButtonPress)
          self.present(alertController, animated: true, completion: nil)
        }
      }
    }
  }
  
  @IBAction func toSignup(_ sender: UIButton) {
    print("sign up")
    performSegue(withIdentifier: "toSignup", sender: self)
  }
  
  func setRootController() {
    let vc = storyboard?.instantiateViewController(withIdentifier: "tabBarController")
    UIApplication.shared.windows.first?.rootViewController = vc
  }
  
}

// MARK: UITextField delegate
extension LoginViewController: UITextFieldDelegate {
  
  // Function to hide keyboard when user taps outside of keyboard
  func hideKeyboardGesture() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
    view.addGestureRecognizer(tap)
  }
  
  @objc func dismissKeyboard() {
    view.endEditing(true)
  }
  
  func textFieldDidBeginEditing(_ textField: UITextField) {

    switch textField.tag {

    case 0:
      print("username field focused")

    case 1:
      print("password field focused")

    default:
      print("unknown tag")
    }

    scrollView.setContentOffset(CGPoint(x: 0, y: 120), animated: true)
    
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    scrollView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
  }

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField.tag == 0 {
      passwordField.becomeFirstResponder()
    } else if textField.tag == 1 {
      textField.resignFirstResponder()
    }
    return true
  }
  
}
