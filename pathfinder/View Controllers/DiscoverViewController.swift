//
//  DiscoverViewController.swift
//  pathfinder
//
//  Created by Niklas Kiuru on 03/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
// ------------------------------------------------------------------------------------------------------------
//   This view controller:
//     1) Fetch/show all main categories
//     2) Fetch/show popular categories
//     3) Determines the category to search courses for
// ------------------------------------------------------------------------------------------------------------

import UIKit

class DiscoverViewController: BaseViewController {
  
  @IBOutlet weak var popularLabel: UILabel!
  @IBOutlet weak var categoriesLabel: UILabel!
  @IBOutlet weak var popularCollectionView: UICollectionView!
  @IBOutlet weak var categoriesCollectionView: UICollectionView!
  @IBOutlet weak var mainViewAllButton: UIButton!
  @IBOutlet weak var popularViewAllButton: UIButton!
  
  var popularCategories: [Categories]? = []
  var mainCategories: [Categories]? = []
  override func viewDidLoad() {
    super.viewDidLoad()
    self.popularLabel.textColor = UIColor.PF.Title
    self.categoriesLabel.textColor = UIColor.PF.Title
    popularViewAllButton.tintColor = UIColor.PF.MainGreen
    mainViewAllButton.tintColor = UIColor.PF.MainGreen
    
    let popularLayout = UICollectionViewFlowLayout()
    popularLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 10)
    popularLayout.itemSize = CGSize(width: 160, height: 120)
    popularLayout.scrollDirection = .horizontal
    
    popularCollectionView.delegate = self
    popularCollectionView.dataSource = self
    popularCollectionView.collectionViewLayout = popularLayout
    
    let categoriesLayout = UICollectionViewFlowLayout()
    categoriesLayout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 10)
    categoriesLayout.itemSize = CGSize(width: 160, height: 120)
    categoriesLayout.scrollDirection = .horizontal
    popularCategories = PFCategoriesFetchPopular()
    mainCategories = PFCategoriesFetchAll()
    categoriesCollectionView.delegate = self
    categoriesCollectionView.dataSource = self
    categoriesCollectionView.collectionViewLayout = categoriesLayout
    
  }
  
  @IBAction func onMainViewButtonTap(_ sender: Any) {
    let categories = mainCategories
    performSegue(withIdentifier: "fromDiscoverToCategories", sender: categories)
  }
  
  @IBAction func onPopularViewAllButtonTap(_ sender: Any) {
    let categories = popularCategories
    performSegue(withIdentifier: "fromDiscoverToCategories", sender: categories)
  }
  
  
  // MARK: - Navigation
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    super.prepare(for: segue, sender: sender)
    
    switch(segue.identifier ?? "") {
      
    case "toSearch":
      guard let searchController = segue.destination as? SearchViewController else {
        fatalError("Unexpected destination: \(segue.destination)")
      }
      
      if let ID = sender as? Int64 {
        print("Setting category id for search to \(ID)")
        searchController.categoryIdForSearch = "\(ID)"
      } 
      
    case "fromDiscoverToCategories":
      let destVC = segue.destination as! CategoriesViewController
      destVC.categoriesToDisplay = sender as? [Categories]
      
    default:
      print("other segue")
    }
  }
}

extension DiscoverViewController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    switch collectionView.tag {
    case 0: //Popular categories
      if let categories = popularCategories {
        print("Popular categories: \(categories.count)")
        if categories.count > 8 {
          return 8
        } else {
          return categories.count
        }
      } else {
        return 0
      }
      
    case 1: // Main categories
      if let categories = mainCategories {
        print("Main categories: \(categories.count)")
        if categories.count > 8 {
          return 8
        } else {
          return categories.count
        }
      } else {
        return 0
      }
      
    default:
      return 0
    }
    
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    switch collectionView.tag {
    case 0:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topicCell", for: indexPath as IndexPath) as! TopicCell
      cell.setTheme()
      let cellTitle = popularCategories?[indexPath.row].category
      cell.setTitle(title: cellTitle!)
      return cell
    case 1:
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topicCell", for: indexPath as IndexPath) as! TopicCell
      cell.setTheme()
      let cellTitle = mainCategories?[indexPath.row].category
      cell.setTitle(title: cellTitle!)
      return cell
    default:
      return UICollectionViewCell()
    }
    
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    
    if collectionView.tag == 0 { // Popular categories
      guard let categoryID = popularCategories?[indexPath.row].id else { fatalError("Failed to get category ID") }
      performSegue(withIdentifier: "toSearch", sender: categoryID)
    }
    
    if collectionView.tag == 1 { // Main categories
      guard let categoryID = mainCategories?[indexPath.row].id else { fatalError("Failed to get category ID") }
      performSegue(withIdentifier: "toSearch", sender: categoryID)
    }
    
  }
  
}
