//
//  BaseViewController.swift
//  pathfinder
//
//  Created by iosdev on 22/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
  override func viewDidLoad() {
    super.viewDidLoad()
    setupColors()
    setupToolbarAppearance()
  }
  
  private func setupColors() {
    view.backgroundColor = UIColor.PF.PageBackground
  }
  
  private func setupToolbarAppearance() {
    
    if let navbar = navigationController {
      
      // Removes light shadow from toolbar
      navbar.navigationBar.shadowImage = UIImage()
      
      navbar.navigationBar.isTranslucent = false
      navbar.navigationBar.tintColor = UIColor.PF.MainGreen
      navbar.navigationBar.barTintColor = UIColor.PF.MainBlue
      
      navbar.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.PF.Title]
      navbar.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.PF.Title]
    }
  }
  
}
