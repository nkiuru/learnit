//
//  PathsViewController.swift
//  pathfinder
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import CoreData

class PathsViewController: BaseViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  private var samplePaths: [[Courses]] = []
  var studyPaths: [StudyPaths] = []
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    getCoursesData()
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = UIColor.PF.PageBackground
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "fromPathsToPreview" {
      let destVC = segue.destination as! PreviewViewController
      let studypath: StudyPaths = (sender as? StudyPaths)!
      let courses = PFFetchStudypathCourses(courses: studypath.path ?? "")
      destVC.courses = courses
      destVC.studypath = studypath
    }
  }
  
  private func getCoursesData() {
    studyPaths.forEach({path in
        samplePaths.append(PFFetchStudypathCourses(courses: path.path ?? ""))
    })
  }
}

extension PathsViewController: UITableViewDelegate, UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 2
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == 0 {
      return 1
    } else {
      return samplePaths.count
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch indexPath.section {
    
    case 0:
      let cell = tableView.dequeueReusableCell(withIdentifier: "instructionCell") as! PreviewInstructionCell
      cell.setTheme()
      return cell
    
    case 1:
      let cell = tableView.dequeueReusableCell(withIdentifier: "pathCell") as! PathCell
      cell.setTheme()
      cell.setData(courses: samplePaths[indexPath.row], name: studyPaths[indexPath.row].name ?? "No name")
      return cell
      
    default:
      return UITableViewCell()
      
    }
    
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.section == 1 {
      let courses = studyPaths[indexPath.row]
        self.performSegue(withIdentifier: "fromPathsToPreview", sender: courses)
    }
  }
  
  
}
