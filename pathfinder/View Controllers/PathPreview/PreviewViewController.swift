//
//  PreviewViewController.swift
//  pathfinder
//
//  Created by iosdev on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class PreviewViewController: UIViewController {
  
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var confirmContainerView: UIView!
  
  var courses: [Courses]?
  var studypath: StudyPaths?
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.delegate = self
    tableView.dataSource = self
    tableView.backgroundColor = UIColor.PF.PageBackground
    confirmContainerView.backgroundColor = UIColor.PF.MainBlue
  }
  
  @IBAction func onConfirmTap(_ sender: Any) {
    if let courses = courses {
        var ids: [Int64] = []
        courses.forEach({course in
            ids.append(course.id)
        })
        PFBackendSaveStudyPath(newStudyPath: ids) {(success, error) in
            if success {
                self.performSegue(withIdentifier: "toHome", sender: nil)
            } else {
                print("[PFBackendSaveStudyPath] \(error)")
            }
        }
    }
  }
  
  func setRootController() {
    let vc = storyboard?.instantiateViewController(withIdentifier: "tabBarController")
    UIApplication.shared.windows.first?.rootViewController = vc
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    super.prepare(for: segue, sender: sender)
    
    if segue.identifier == "fromPreviewToCourseInfo" {
      let destVC = segue.destination as! CourseInfoViewController
      destVC.course = sender as? Courses
      destVC.isPreview = true
    }
  }
  
}

extension PreviewViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let courses = courses {
      return courses.count
    } else {
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteCell") as! FavouriteCell
    guard let course = courses?[indexPath.row] else { fatalError("Course data error") }
    cell.setData(data: course)
    cell.setTheme()
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let course = courses?[indexPath.row] else { fatalError("Course data error") }
    performSegue(withIdentifier: "fromPreviewToCourseInfo", sender: course)
  }
  
}
