//
//  CategoriesViewController.swift
//  pathfinder
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class CategoriesViewController: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var categoriesToDisplay: [Categories]? = nil
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    collectionView.delegate = self
    collectionView.dataSource = self
    
    collectionView.backgroundColor = UIColor.PF.PageBackground
    
    let layout = UICollectionViewFlowLayout()
    layout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    let cellWidth = view.bounds.width / 2 - 24
    layout.itemSize = CGSize(width: cellWidth, height: 125)
    layout.minimumLineSpacing = 16
    layout.minimumInteritemSpacing = 16
    layout.scrollDirection = .vertical
    collectionView.collectionViewLayout = layout
    
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "fromCategoriesToSearch" {
      let destVC = segue.destination as! SearchViewController
      if let ID = sender as? Int64 {
        destVC.categoryIdForSearch = "\(ID)"
      }
    }
  }
  
}

extension CategoriesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    if let categories = categoriesToDisplay {
      return categories.count
    } else {
      return 0
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "topicCell", for: indexPath as IndexPath) as! TopicCell
    cell.setTheme()
    guard let cellTitle = categoriesToDisplay?[indexPath.row].category else { fatalError("Failed to get category") }
    cell.setTitle(title: cellTitle)
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let categoryID = categoriesToDisplay?[indexPath.row].id else { fatalError("Failed to get category ID") }
    performSegue(withIdentifier: "fromCategoriesToSearch", sender: categoryID)
  }
  
}
