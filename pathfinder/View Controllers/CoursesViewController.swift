//
//  FavouritesViewController.swift
//  pathfinder
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import CoreData

public enum CoursesTypes: String {
    case Enrolled, Finished
}

class CoursesViewController: BaseViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let TO_COURSE_INFO_IDENTIFIER = "CoursesToCourseInfo"
    var coursesToShow: CoursesTypes? = nil
    var fetchedResultsController: NSFetchedResultsController<Courses>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let courseType = coursesToShow {
            self.title = courseType.rawValue
            let fetchRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
            switch courseType {
            case .Enrolled:
                let predicate1 = NSPredicate(format: "enrolled == %d", 1)
                let predicate2 = NSPredicate(format: "finished == %d", 0)
                fetchRequest.predicate = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
                let sortDescriptor = NSSortDescriptor(key: "enroll_time", ascending: true)
                fetchRequest.sortDescriptors = [sortDescriptor]
            case .Finished:
                fetchRequest.predicate = NSPredicate(format: "finished == %d", 1)
                let sortDescriptor = NSSortDescriptor(key: "finish_time", ascending: true)
                fetchRequest.sortDescriptors = [sortDescriptor]
            }
            tableView.delegate = self
            tableView.dataSource = self
            //fetchCourses()
            fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: PersistenceService.context, sectionNameKeyPath: nil, cacheName: nil)
            fetchedResultsController?.delegate = self as NSFetchedResultsControllerDelegate
            try? fetchedResultsController?.performFetch()
        } else {
            print("[CoursesViewController] You must pass course type")
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == TO_COURSE_INFO_IDENTIFIER {
            let destVC = segue.destination as! CourseInfoViewController
            destVC.course = sender as? Courses
        }
    }
    
//    private func fetchCourses() {
//        // Fetch data from the server & display loading spinner if updating manually
//        APIData(load: .Courses) { (success, error) in
//            print("[CoursesViewController] APIDATA[load: .Courses] Success=\(success) \(error)")
//            if success {
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
//            }
//        }
//    }
    // NSFetchedResultsControllerDelegate code:
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
        print("Begin updating")
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        print("End updating")
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        default:
            print("...")
        }
    }
}

// MARK: UITableView code
extension CoursesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IDENTIFIER.COURSES_ITEM, for: indexPath) as? CourseCell else {
            fatalError(":(")
        }
        guard let course = self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Item not found")
        }
        cell.setTheme()
        cell.setData(data: course)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController!.sections?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Perform a segue to course info when row tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let course = self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Item not found")
        }
        performSegue(withIdentifier: TO_COURSE_INFO_IDENTIFIER, sender: course)
    }
    
}
