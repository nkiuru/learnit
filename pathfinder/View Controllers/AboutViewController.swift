//
//  AboutViewController.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 06/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import SafariServices

class AboutViewController: UIViewController {

    @IBOutlet var uiViewBase: UIView!
    @IBOutlet weak var lblCopyright: UILabel!
    @IBOutlet weak var btnGitLab: UIButton!
    @IBOutlet weak var btnTrello: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        uiViewBase.backgroundColor = UIColor.PF.PageBackground
        lblCopyright.textColor = UIColor.PF.LabelText
    }
    @IBAction func buttonGitLab(_ sender: UIButton) {
        guard let providerURL = URL(string: "https://gitlab.com/nkiuru/learnit") else { fatalError("URL error") }
        let safariVC = SFSafariViewController(url: providerURL)
        present(safariVC, animated: true, completion: nil)
    }
    
    @IBAction func buttonTrello(_ sender: UIButton) {
        guard let providerURL = URL(string: "https://trello.com/b/DEvi6wZm/super-squab") else { fatalError("URL error") }
        let safariVC = SFSafariViewController(url: providerURL)
        present(safariVC, animated: true, completion: nil)
    }
    
}
