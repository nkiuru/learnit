//
//  OnboardingViewController.swift
//  pathfinder
//
//  Created by Sara Suviranta on 24/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class OnboardingViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate {

  @IBOutlet weak var welcomeMsg: UILabel!
  var categories = [Categories?]()
  
  @IBOutlet weak var collectionView: UICollectionView!
  
  var chosenCats = [Categories]()
  static var skillLevel: Int = 666
  var paths: [StudyPaths]? = []

  override func viewDidLoad() {
    super.viewDidLoad()
    self.navigationItem.leftBarButtonItem = nil
    self.navigationItem.hidesBackButton = true
    
    categories = PFCategoriesFetchOnboarding()
    if (collectionView != nil) {
      let layout = UICollectionViewFlowLayout()
      let cellWidth = view.bounds.width / 2 - 24
      layout.itemSize = CGSize(width: cellWidth, height: 125)
      layout.minimumLineSpacing = 16
      layout.minimumInteritemSpacing = 16
      layout.scrollDirection = .vertical
      collectionView.collectionViewLayout = layout
    }
  }

  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return categories.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "obCell", for: indexPath) as! OnboardingCell
  
    cell.setTheme()

    if let category = categories[indexPath.row]{
      if chosenCats.contains(category) {
        cell.obLabel.textColor = UIColor.PF.MainBlue
        cell.backgroundColor = UIColor.PF.MainGreen
      } else {
        cell.obLabel.textColor = UIColor.PF.MainGreen
        cell.backgroundColor = UIColor.PF.Card
      }
    }
    
    cell.obLabel.text = categories[indexPath.row]?.category ?? "Unknown"
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let cell = collectionView.cellForItem(at: indexPath) as! OnboardingCell
  
    if let category = categories[indexPath.row]{
      if !chosenCats.contains(category) {
        chosenCats.append(category)
        cell.obLabel.textColor = UIColor.PF.MainBlue
        cell.backgroundColor = UIColor.PF.MainGreen
      } else {
        chosenCats.removeAll(where: { $0 == category })
        cell.obLabel.textColor = UIColor.PF.MainGreen
        cell.backgroundColor = UIColor.PF.Card
      }
    }
  }
  
  @IBAction func continueBtn(_ sender: UIButton) {
    performSegue(withIdentifier: "toSkillLevel", sender: self)
  }
  
  @IBAction func beginnerBtn(_ sender: UIButton) {
    OnboardingViewController.skillLevel = 0
    performSegue(withIdentifier: "toInterests", sender: self)
  }
  
  @IBAction func intermediateBtn(_ sender: UIButton) {
    OnboardingViewController.skillLevel = 1
    performSegue(withIdentifier: "toInterests", sender: self)
  }
  
  @IBAction func advancedBtn(_ sender: UIButton) {
    OnboardingViewController.skillLevel = 2
    performSegue(withIdentifier: "toInterests", sender: self)
  }
  
  func canCreateStudypath() -> Bool {
    if OnboardingViewController.skillLevel != 666 && chosenCats.count >= 5{
      return true
    } else {
      return false
    }
  }
  
  @IBAction func createPathBtn(_ sender: UIButton) {
    print("SKILL LEVEL: \(OnboardingViewController.skillLevel)")
    print("CHOSEN CATS: \(chosenCats.count)")
    let ok = canCreateStudypath()

    if !ok {
      let alertController = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: NSLocalizedString("Select 5-10 topics", comment: ""), preferredStyle: .alert)
      let actionOKButtonPress = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction) in
        // Action if needed.
      }
      alertController.addAction(actionOKButtonPress)
      self.present(alertController, animated: true, completion: nil)
    } else {
      let suggester = StudypathSuggester()
      paths = suggester.suggestStudypath(chosenCategories: chosenCats, skillLevel: OnboardingViewController.skillLevel)
      performSegue(withIdentifier: "toPathPreview", sender: self)
    }

  }
  
  func setRootController() {
    let vc = storyboard?.instantiateViewController(withIdentifier: "tabBarController")
    UIApplication.shared.windows.first?.rootViewController = vc
  }
    
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    super.prepare(for: segue, sender: sender)
    
    switch(segue.identifier ?? "") {
        
    case "toPathPreview":
        guard let pathController = segue.destination as? PathsViewController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        pathController.studyPaths = paths ?? []
    default:
        print("other segue")
    }
  }
}


