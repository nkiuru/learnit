//
//  FavouritesViewController.swift
//  pathfinder
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
// ------------------------------------------------------------------------------------------------------------
//   This view controller:
//     1) Fetches favourites from courses core data
//     2) Displays favourited courses
// ------------------------------------------------------------------------------------------------------------

import UIKit
import CoreData

class FavouritesViewController: BaseViewController, NSFetchedResultsControllerDelegate {
  
  @IBOutlet weak var tableView: UITableView!
  //private let refreshControl = UIRefreshControl()

  let TO_COURSE_INFO_IDENTIFIER = "FavouritesToCourseInfo"
    
  var fetchedResultsController: NSFetchedResultsController<Courses>?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Setup pull-to-refresh. Does not work with large titles reliably
//    if #available(iOS 10.0, *) {
//        tableView.refreshControl = refreshControl
//    } else {
//        tableView.addSubview(refreshControl)
//    }
//    refreshControl.addTarget(self, action: #selector(refreshCourseData(_:)), for: .valueChanged)
    tableView.delegate = self
    tableView.dataSource = self
    //fetchFavourites()
    let fetchRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
    fetchRequest.predicate = NSPredicate(format: "favourited == %d", 1)
    fetchRequest.sortDescriptors = [sortDescriptor]
    fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: PersistenceService.context, sectionNameKeyPath: nil, cacheName: nil)
    fetchedResultsController?.delegate = self as NSFetchedResultsControllerDelegate
    try? fetchedResultsController?.performFetch()
  }
    
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    super.prepare(for: segue, sender: sender)
    
    if segue.identifier == TO_COURSE_INFO_IDENTIFIER {
      let destVC = segue.destination as! CourseInfoViewController
      destVC.course = sender as? Courses
    }
  }
//
//    @objc func refreshCourseData(_ sender: Any) {
//        // Fetch Course Data
//        fetchFavourites()
//    }
    
//    private func fetchFavourites() {
//        // Fetch data from the server & display loading spinner if updating manually
//        APIData(load: .Courses) { (success, error) in
//            print("[FavouritesViewController] APIDATA[load: .Courses] Success=\(success) \(error)")
//            if success {
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                    //self.refreshControl.endRefreshing()
//                }
//            }
//        }
//    }
    // NSFetchedResultsControllerDelegate code:
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
        //print("Begin updating")
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        //print("End updating")
        tableView.reloadData()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break;
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break;
        default:
            print("...")
        }
    }
}

// MARK: UITableView code
extension FavouritesViewController: UITableViewDataSource, UITableViewDelegate {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            return sections[section].numberOfObjects
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: IDENTIFIER.FAVOURITE_ITEM, for: indexPath) as? FavouriteCell else {
            fatalError(":(")
        }
        guard let fav = self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Item not found")
        }
        cell.setTheme()
        cell.setData(data: fav)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController!.sections?.count ?? 1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            guard let course = self.fetchedResultsController?.object(at: indexPath) else {
                fatalError("Item not found")
            }
            let courseID: Int64 = course.id
            PFFavoriteRemove(courseID: courseID) { (success, error) in
                if (success) {
                    if updateCourse(id: course.id, key: "favourited", value: 0).0 {
                        //tableView.deleteRows(at: [indexPath], with: .automatic)
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    } else {
                        print("ERROR")
                    }
                }
            }
        }
    }
  
  // Perform a segue to course info when row tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let course = self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Item not found")
        }
        performSegue(withIdentifier: TO_COURSE_INFO_IDENTIFIER, sender: course)
    }
  
}
