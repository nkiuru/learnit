//
//  SecondViewController.swift
//  pathfinder
//
//  Created by iosdev on 16/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import CoreData

private struct SECTION {
  static let SUMMARY = 0
  static let TOPICS = 1
  static let JOURNEY = 2
}

class HomeViewController: BaseViewController, NSFetchedResultsControllerDelegate, TopicCellSelectionDelegate {
  
  @IBOutlet weak var tableView: UITableView!
    let toCourseInfoIdentifier = "HomeToCourseInfo"

  var info = PFMyInfoCalculate()
  var fetchedResultsController: NSFetchedResultsController<Courses>?
  var topics:[Categories]? = nil
    
  let sections: [String?] = [
    nil, // Summary
    nil, //Topics for you
    "Your journey"
  ]
  
  override func viewDidLoad() {
      super.viewDidLoad()
      PFBackendFetchLoginEssential()
        
      tableView.delegate = self
      tableView.dataSource = self
      tableView.backgroundColor = UIColor.PF.PageBackground
      topics = PFCategoriesFetchChildren(id: PFFetchStudypathCategory() ?? 0)

        let fetchRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "studypath", ascending: true)
        fetchRequest.predicate = NSPredicate(format: "studypath >= %d", 1)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: PersistenceService.context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController?.delegate = self as NSFetchedResultsControllerDelegate
        try? fetchedResultsController?.performFetch()
    }
    
    func didSelect(id: Int) {
        performSegue(withIdentifier: "toSearch", sender: id)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        switch segue.identifier {
        case toCourseInfoIdentifier:
            let destVC = segue.destination as! CourseInfoViewController
            destVC.course = sender as? Courses
        case "toSearch":
            guard let searchController = segue.destination as? SearchViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            if let ID = sender as? Int {
                let catId = topics?[ID].id ?? 0
                print("Setting category id for search to \(catId)")
                searchController.categoryIdForSearch = "\(catId)"
            }
        default:
            print("other segue")
        }
    }
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        //print("End updating")
        topics = PFCategoriesFetchChildren(id: PFFetchStudypathCategory() ?? 0)
        info = PFMyInfoCalculate()
        tableView.reloadData()
    }
}

// MARK: UITableView delegates
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
 
  func numberOfSections(in tableView: UITableView) -> Int {
    return sections.count
  }
  
  // Setup header cell
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
    if let sectionName = sections[section] {
      let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! HomeHeaderCell
      cell.setTheme()
      cell.setHeader(value: NSLocalizedString(sectionName, comment: ""))
      return cell
    }
    
    return nil
  }
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    if section == SECTION.JOURNEY {
      return 70
    } else {
      return 0
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == SECTION.JOURNEY {
      return fetchedResultsController?.fetchedObjects?.count ?? 0
    } else{
      return 1
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    switch indexPath.section {
    case SECTION.SUMMARY:
      info = PFMyInfoCalculate()
      let cell = tableView.dequeueReusableCell(withIdentifier: "summaryCard") as! SummaryCell
      cell.setTheme()
      cell.setData(enrolledCredits: info.courseECTSEnrolled, finishedCredits: info.courseECTSGathered, courseCountEnrolled: info.courseCountEnrolled, courseCountFinished: info.courseCountFinished)
      return cell
      
    case SECTION.TOPICS:
      let cell = tableView.dequeueReusableCell(withIdentifier: "TopicsForYou") as! TopicsCell
      cell.topics = topics
      cell.delegate = self
      cell.setTheme()
      return cell
    
    case SECTION.JOURNEY:
      let cell = tableView.dequeueReusableCell(withIdentifier: "journeyCell") as! JourneyItemCell
      //let index = indexPath.row - 3
      guard let course = self.fetchedResultsController?.object(at: [0, indexPath.row]) else {
        fatalError("Item not found")
      }
      cell.setData(data: course)
      cell.setTheme()
      return cell
      
    default:
      return UITableViewCell()
    }

  }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("[homeViewController] Clicked Path Item # \(indexPath)")
        if indexPath.section == SECTION.JOURNEY {
            guard let course = self.fetchedResultsController?.object(at: [0,indexPath.row]) else {
                fatalError("item not found")
            }
            performSegue(withIdentifier: toCourseInfoIdentifier, sender: course)
        }
    }
  
}
