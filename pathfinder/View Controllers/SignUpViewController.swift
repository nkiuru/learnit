//
//  RegisterViewController.swift
//  pathfinder
//
//  Created by Sara Suviranta on 24/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class SignUpViewController: BaseViewController {
  
  
  @IBOutlet weak var scrollView: UIScrollView!
  @IBOutlet weak var usernameField: PFTextField!
  @IBOutlet weak var emailField: PFTextField!
  @IBOutlet weak var fullNameField: PFTextField!
  @IBOutlet weak var passwordField: PFTextField!
  @IBOutlet weak var rePasswordField: PFTextField!
  var validPassword = ""
  var validEmail = ""
  var validUsername = ""
  var validFullname = ""
  var success = false
  var parameters: [String: String] = [:]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    hideKeyboardGesture()
    scrollView.backgroundColor = UIColor.PF.PageBackground
    passwordField.isSecureTextEntry = true;
    rePasswordField.isSecureTextEntry = true;
  }
  
  func errorMsg(title: String, msg: String) {
    let alertCtrl = UIAlertController(title: title, message: msg, preferredStyle: .alert)
    let continueBtn = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction) in
      // Action if needed.
    }
    alertCtrl.addAction(continueBtn)
    self.present(alertCtrl, animated: true, completion: nil)
  }
  
  func isValidEmail(emailString: String) -> Bool {
    var isValid = false
    let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
    
    do {
      let regex = try NSRegularExpression(pattern: emailRegEx)
      let nsString = emailString as NSString
      let result = regex.matches(in: emailString, range: NSRange(location: 0, length: nsString.length))
      
      if result.count == 1 {
        isValid = true
      }
    } catch let err as NSError {
      print("invalid email: \(err.localizedDescription)")
    }
    return isValid
  }
  
  func register(callback: @escaping ()->()) {
    guard let username = usernameField.text, !username.isEmpty else {
      removeSpinner()
      errorMsg(title: "Missing credentials", msg: "Username is required")
      return
    }
    parameters["username"] = username
    
    guard let password = passwordField.text, !password.isEmpty else {
      errorMsg(title: "Missing credentials", msg: "Password is required")
      return
    }
    
    guard let rePassword = rePasswordField.text, !rePassword.isEmpty else {
      errorMsg(title: "Missing credentials", msg: "Re-Password is required")
      return
    }
    
    if password == rePassword {
      validPassword = password
    } else {
      errorMsg(title: "Invalid credentials", msg: "Passwords do not match")
    }
    parameters["password"] = validPassword
    
    if let fullName = fullNameField.text, !fullName.isEmpty {
      parameters["full_name"] = fullName
    }
  
    if let email = emailField.text, !email.isEmpty {
      let isValid = isValidEmail(emailString: email)
      
      if !isValid {
        errorMsg(title: "Invalid credentials", msg: "Invalid email address")
        return
      } else {
        parameters["email"] = email
      }
    }
    self.showSpinner(onView: self.view)
    print(parameters)
    
    
    JSONRequest(operation: "register", parameters: parameters) { (response) in
      if !response.success {
        print(response.error)
        self.removeSpinner()
        // TODO: ALERT ERROR MSG
        // for some reason errorMsg() is not working here
      } else {
        self.success = true
        self.removeSpinner()
        self.loginNewUser(username: username, password: self.validPassword, callback: callback)
      }
    }
    self.removeSpinner()
  }
  
  
  func loginNewUser(username: String, password: String, callback: @escaping () -> ()) {
    self.showSpinner(onView: self.view)

    JSONRequest(operation: "login", parameters: ["username": username, "password": password]) { (response) in
      if !response.success {
        print("log in failed")
        let alertController = UIAlertController(title: NSLocalizedString("Login Failed", comment: ""), message: response.error, preferredStyle: .alert)
        let actionOKButtonPress = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            // Action if needed.
        }
        alertController.addAction(actionOKButtonPress)
        self.present(alertController, animated: true, completion: nil)
      } else {
        DispatchQueue.main.async {
          print(response.success)
          self.removeSpinner()
          let _ = PFLoginSaveUserToCD(jsonResponse: response)
            PFBackendFetchLoginEssential()
          callback()
        }
      }
    }
  }
  
  @IBAction func signUpBtn(_ sender: UIButton) {
    register(callback: {
      if self.success {
        print("succcess")
        self.performSegue(withIdentifier: "toOnboarding", sender: self)
      } else {
        print("failed")
      }
    })
  }
}

extension SignUpViewController: UITextFieldDelegate {
  
  // Function to hide keyboard when user taps outside of keyboard
  func hideKeyboardGesture() {
    let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
    view.addGestureRecognizer(tap)
  }
  
  @objc func dismissKeyboard() {
    view.endEditing(true)
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    if textField.tag == 0 {
      emailField.becomeFirstResponder()
    } else if textField.tag == 1 {
      fullNameField.becomeFirstResponder()
    } else if textField.tag == 2 {
      passwordField.becomeFirstResponder()
    } else if textField.tag == 3 {
      rePasswordField.becomeFirstResponder()
    } else if textField.tag == 4 {
      textField.resignFirstResponder()
    }
    return true
  }
  
}
