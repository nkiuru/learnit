//
//  FirstViewController.swift
//  pathfinder
//
//  Created by iosdev on 16/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit
import CoreData

class ProfileViewController: BaseViewController, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var info = PFMyInfoCalculate()
    var fetchedResultsController: NSFetchedResultsController<Courses>?

    override func viewDidLoad() {
        super.viewDidLoad()
        //nameField.textColor = UIColor.PF.Title
        //emailField.textColor = UIColor.PF.Text
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.PF.PageBackground
        info = PFMyInfoCalculate()
        let fetchRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "studypath", ascending: true)
        fetchRequest.predicate = NSPredicate(format: "studypath >= %d", 1)
        fetchRequest.sortDescriptors = [sortDescriptor]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: PersistenceService.context, sectionNameKeyPath: nil, cacheName: nil)
        fetchedResultsController?.delegate = self as NSFetchedResultsControllerDelegate
        try? fetchedResultsController?.performFetch()
    }

  @IBAction func logout(_ sender: UIButton) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: NSLocalizedString("Logout", comment: ""), message: NSLocalizedString("Are you sure you want to logout?", comment: ""), preferredStyle: .alert)
            let actionLogoutButtonPress = UIAlertAction(title: NSLocalizedString("Logout", comment: ""), style: .destructive) { (action:UIAlertAction) in
                self.showSpinner(onView: self.view)
                JSONRequest(operation: "logout", parameters: nil, parseFunction: { (response) in
                    print("JSONRequest 'logout' response was: \(response.success) \(response.error)")
                    PFLoginSetStateLogout()
                    DispatchQueue.main.async {
                        self.gotoLoginSegue()
                    }
                    self.removeSpinner()
                })
            }
            let actionCancelButtonPress = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel) { (action:UIAlertAction) in
                print("Logout: User cancelled.")
                self.removeSpinner()
            }
            alertController.addAction(actionCancelButtonPress)
            alertController.addAction(actionLogoutButtonPress)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func gotoLoginSegue() {
        // TODO: Add transition to segue
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "loginSignupCtrl")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = vc
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        switch(segue.identifier ?? "") {
            
        case "toEnrolledCourses":
            guard let coursesController = segue.destination as? CoursesViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            coursesController.coursesToShow = .Enrolled
        case "toFinishedCourses":
            guard let coursesController = segue.destination as? CoursesViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            coursesController.coursesToShow = .Finished
        case "ProfileToCourseInfo":
            guard let courseInfoController = segue.destination as? CourseInfoViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            courseInfoController.course = sender as? Courses

        default:
            print("other segue")
        }
    }
    
    // NSFetchedResultsControllerDelegate code:
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
        //print("Begin updating")
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
        //print("End updating")
        info = PFMyInfoCalculate()
        tableView.reloadData()
    }
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            print("[ProfileViewController] Clicked User Info")
        case 1:
            print("[ProfileViewController] Clicked Credits Section")
        case 2:
            print("[ProfileViewController] Clicked Enrolled Courses")
            performSegue(withIdentifier: "toEnrolledCourses", sender: nil)
        case 3:
            print("[ProfileViewController] Clicked Finished Courses")
            performSegue(withIdentifier: "toFinishedCourses", sender: nil)
        case 4:
            print("[ProfileViewController] Clicked MyPath -title")
        default:
            let pathItemIndex = indexPath.row - 5
            print("[ProfileViewController] Clicked Path Item # \(pathItemIndex)")
            guard let course = self.fetchedResultsController?.object(at: [0,indexPath.row-5]) else {
                fatalError("Item not found")
            }
            performSegue(withIdentifier: "ProfileToCourseInfo", sender: course)


        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController!.sections?.count ?? 1
    }

    // Header height
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = fetchedResultsController?.sections, sections.count > 0 {
            let rows = (5 + sections[section].numberOfObjects)
            return rows
        }
        return 4
    }

    // Setion cell and setup
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0: // User Info Cell (ProfileUserInfoTableViewCell)
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileUserInfoTableViewCell") as! ProfileUserInfoTableViewCell
            
            // Fetch current login information from PFLogin
            if let user = PFLoginUser {
                cell.setData(fullName: user.fullname ?? "", email: user.email ?? "")
            }
            
            cell.setTheme()
            return cell
            
        case 1: // (ProfileCreditsTableViewCell)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCreditsTableViewCell") as! ProfileCreditsTableViewCell
            cell.setData(enrolledCredits: info.courseECTSEnrolled, finishedCredits: info.courseECTSGathered)
            cell.setTheme()
            return cell
            
            
        case 2...3: // (ProfileCourseItemTableViewCell)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCourseItemTableViewCell") as! ProfileCourseItemTableViewCell
            if indexPath.row == 2 {
                cell.setData(title: NSLocalizedString("Enrolled Courses", comment: ""), count: String(info.courseCountEnrolled))
            } else {
                cell.setData(title: NSLocalizedString("Finished Courses", comment: ""), count: String(info.courseCountFinished))
            }
            
            cell.setTheme()
            return cell
        
        case 4: // (ProfileMyPathTitleTableViewCell)
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMyPathTitleTableViewCell") as! ProfileMyPathTitleTableViewCell
            
            
            cell.setTheme()
            return cell
            
        case 5...: // (ProfileMyPathItemTableViewCell)
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileMyPathItemTableViewCell") as! ProfileMyPathItemTableViewCell
            let index = indexPath.row - 5
            guard let course = self.fetchedResultsController?.object(at: [0, index]) else {
                fatalError("Item not found")
            }
            cell.setData(data: course)
            cell.setTheme()
            
            return cell
            
        default:
            fatalError("WTF just happened")
        }
        
    }
    
}
