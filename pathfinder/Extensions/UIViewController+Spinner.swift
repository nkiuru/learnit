//
//  UIViewController+Spinner.swift
//  pathfinder
//
//  Created by iosdev on 26/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
// ------------------------------------------------------------------------------------------------------------
//  Extension for uiviewcontroller for showing and removing a transparent spinner overlay that blocks user input
// ------------------------------------------------------------------------------------------------------------

import Foundation
import UIKit

var vSpinner: UIView?

extension UIViewController {
    func showSpinner(onView : UIView) {
        DispatchQueue.main.async {
            let spinnerView = UIView.init(frame: onView.bounds)
            if !UIAccessibility.isReduceTransparencyEnabled {
                // TODO: better blurring effect on the background of the spinner
                
                //            let blurEffect = UIBlurEffect(style: .dark)
                //            let blurEffectView = UIVisualEffectView(effect: blurEffect)
                //            //always fill the view
                //            blurEffectView.frame = self.view.bounds
                //            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
                //
                //            view.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
            }
            let ai = UIActivityIndicatorView.init(style: .whiteLarge)
            ai.startAnimating()
            ai.center = spinnerView.center
            
            DispatchQueue.main.async {
                spinnerView.addSubview(ai)
                onView.addSubview(spinnerView)
            }
            vSpinner = spinnerView
        }
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}
