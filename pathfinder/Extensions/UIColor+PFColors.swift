//
//  UIColor+PFColors.swift
//  pathfinder
//
//  Created by iosdev on 21/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

extension UIColor {
  
  struct PF {
    static let MainBlue = UIColor(hex: 0x2F2F44)
    static let MainGreen = UIColor(hex: 0x00DAB3)
    static let PageBackground = UIColor(hex: 0x35354C)
    static let Title = UIColor(hex: 0x78EDDF)
    static let Text = UIColor(hex: 0x7999A5)
    static let Card = UIColor(hex: 0x3E3E59)
    
    // Small Action Button Background
    static let SABBackground = UIColor(white: 1, alpha: 0.07)
    
    static let Label = UIColor(white: 1, alpha: 0.12)
    static let LabelText = UIColor(hex: 0xB8B8C0)
    static let InactiveTab = UIColor(hex: 0x7C7C9D)
    static let EnrolledYellow = UIColor(hex: 0xF8E17E)
  }
  
}
