//
//  SimpleAlert.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 25/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class SimpleAlert {

    public func simpleAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let actionOKButtonPress = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            // Action if needed.
        }
        alertController.addAction(actionOKButtonPress)
        self.present(alertController, animated: true, completion: nil)
    }

}
