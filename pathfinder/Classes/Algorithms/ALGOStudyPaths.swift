//
//  ALGOStudyPaths.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//  This module stores defines some studypaths and stores them into Core Data.
// ------------------------------------------------------------------------------------------------------------

import Foundation
import CoreData

class StudyPathItem {
    public var category: Int64?
    public var level: Int64?
    public var name: String?
    public var path: String?
    public var descr: String?
    public var id: Int64?
    init(id: Int64, category: Int64, level: Int64, name: String, path: String, descr: String) {
        self.category = category
        self.level = level
        self.name = name
        self.path = path
        self.descr = descr
        self.id = id
    }
}

func ALGOStudyPathsLoadIntoCoreData() {
    
    // 1    Programming
    ALGOStudyPathAddItem(item: StudyPathItem(id: 1, category: 1, level: 0, name: "Learn yourself as an IT Mobile Professional in Metropolia", path: "62", descr: "4 years in Metropolia and you will know everything!"))
    ALGOStudyPathAddItem(item: StudyPathItem(id: 2, category: 1, level: 1, name: "Self study online and extend your skills", path: "5, 41, 4", descr: "Extend your skills in JAVA, React and Redux."))
    
    // 43   Databases
    ALGOStudyPathAddItem(item: StudyPathItem(id: 3, category: 43, level: 0, name: "Get to know SQL", path: "32, 33", descr: ""))
    
    // 59   Software
    ALGOStudyPathAddItem(item: StudyPathItem(id: 4, category: 59, level: 0, name: "Learn to use Windows", path: "14", descr: ""))
    
    // 79   Mobile Development
    ALGOStudyPathAddItem(item: StudyPathItem(id: 5, category: 79, level: 2, name: "Become Professional Mobile Developer", path: "40, 59, 39", descr: ""))
    
    // 85   Machine Learning
    ALGOStudyPathAddItem(item: StudyPathItem(id: 6, category: 85, level: 0, name: "Basics of Machine Learning", path: "51, 37", descr: ""))
    
    // 88   IOT
    ALGOStudyPathAddItem(item: StudyPathItem(id: 7, category: 88, level: 0, name: "Get familiar with IOT", path: "18, 26, 54, 49", descr: ""))
    
    // 92   Data Analytics
    ALGOStudyPathAddItem(item: StudyPathItem(id: 8, category: 92, level: 0, name: "DSP Basics", path: "92", descr: ""))
    
    // 94   Web Development
    ALGOStudyPathAddItem(item: StudyPathItem(id: 9, category: 94, level: 2, name: "Become Professional Web Developer", path: "40, 59, 39", descr: ""))
    
    // 102  Security & Safety
    ALGOStudyPathAddItem(item: StudyPathItem(id: 10, category: 102, level: 0, name: "What is safety?", path: "42", descr: ""))
    ALGOStudyPathAddItem(item: StudyPathItem(id: 11, category: 102, level: 1, name: "What is safety even more?", path: "16, 30", descr: ""))
}

private func ALGOStudyPathAddItem(item: StudyPathItem) {
    PersistenceService.context.performAndWait {
        if let sp = try? StudyPaths.createStudyPath(id: item.id ?? 0, context: PersistenceService.context) {
            if let sp = sp {
                sp.category = item.category ?? 0
                sp.descr = item.descr ?? ""
                sp.level = item.level ?? 0
                sp.name = item.name ?? ""
                sp.path = item.path ?? ""
                sp.id = item.id ?? 0
            }
        }
    }
}
