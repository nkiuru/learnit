//
//  PFMyInfo.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 02/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//   This module has functions to calculate My Information Statistics based on Core Data
// ------------------------------------------------------------------------------------------------------------


import Foundation
import CoreData

func PFMyInfoCalculate () -> PFMyInfoObject {
    
    let info: PFMyInfoObject = PFMyInfoObject()
    
    info.courseCountEnrolled = Int32(getEnrolledCount())
    info.courseCountFinished = Int32(getFinishedCount())
    info.courseECTSEnrolled = Int32(getECTSEnrolled())
    info.courseECTSGathered = Int32(getECTSFinished())
    
    return info
}


private func getEnrolledCount() -> Int {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    let predicate1 = NSPredicate(format: "enrolled == %d", 1)
    let predicate2 = NSPredicate(format: "finished == %d", 0)
    courseRequest.predicate = NSCompoundPredicate.init(type: .and, subpredicates: [predicate1,predicate2])
    if let courses = try? PersistenceService.context.fetch(courseRequest) {
        return courses.count
    }
    return 0
}

private func getFinishedCount() -> Int {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    courseRequest.predicate = NSPredicate(format: "finished == %d", 1)
    if let courses = try? PersistenceService.context.fetch(courseRequest) {
        return courses.count
    }
    return 0
}

private func getECTSEnrolled() -> Int {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    courseRequest.predicate = NSPredicate(format: "enrolled == %d", 1)
    if let courses = try? PersistenceService.context.fetch(courseRequest) {
        var credits = 0
        for course in courses {
            credits += Int(course.ects)
        }
        return credits
    }
    return 0
}

private func getECTSFinished() -> Int {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    courseRequest.predicate = NSPredicate(format: "finished == %d", 1)
    if let courses = try? PersistenceService.context.fetch(courseRequest) {
        var credits = 0
        for course in courses {
            credits += Int(course.ects)
        }
        return credits
    }
    return 0
}
