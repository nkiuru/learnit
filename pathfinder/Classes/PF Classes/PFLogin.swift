//
//  PFLogin.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 26/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//   This module holds Login Information variables and Session ID + TOKEN (for app runtime, when app is open).
//   When app is loaded, variables are loaded from CoreData Storage (if session already exists) and put here.
// ------------------------------------------------------------------------------------------------------------

import Foundation
import CoreData

private var LoginExists = false
private var LoginUserID = -1
private var LoginUserName = ""
private var LoginUserFullName = ""
private var LoginUserEmail = ""
private var LoginSessionID = -1
private var LoginSessionToken = ""
private var CurrentUser: User?

func PFLoginSetStateLogin(user:User) {
    LoginExists = true
    CurrentUser = user
    LoginUserID = Int(user.id)
    LoginUserName = user.username ?? ""
    LoginUserFullName = user.fullname ?? ""
    LoginUserEmail = user.email ?? ""
    LoginSessionID = Int(user.session?.id ?? -1)
    LoginSessionToken = user.session?.token ?? ""
}
// During logout delete user, session & courses from core data
func PFLoginSetStateLogout() {
    if let user = CurrentUser {
        if let session = user.session {
            PersistenceService.context.delete(session)
        }
        PersistenceService.context.delete(user)
    }
    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Courses")
    
    // Create Batch Delete Request
    let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
    
    do {
        try PersistenceService.context.execute(batchDeleteRequest)
        
    } catch {
        // Error Handling
    }
    CurrentUser = nil
    LoginExists = false
}

func PFLoginFetchUserFromCD() -> User?{
    let userRequest: NSFetchRequest<User> = User.fetchRequest()
    //userRequest.predicate = NSPredicate(format: "id = %d", id)
    if let user = try? PersistenceService.context.fetch(userRequest) {
        if user.count > 0 {
            return user[0]
        } else if (user.count > 0) {
            print("[PFLogin:PFLoginFetchUserFromCD] Duplicates in core data")
        } else {
            return nil
        }
    }
    return nil
}

// Saves user & session to core data for future faster login
func PFLoginSaveUserToCD(jsonResponse: JSONRequestResponse) -> (Bool, String){
    if let loggedInUser = PFLoginFetchUserFromCD() {
        print("[PFLogin:PFLoginSaveUserToCD] LoggedInUser: \(loggedInUser)")
        LoginExists = true
        CurrentUser = loggedInUser
    }
    let id: Int64 = Int64(jsonResponse.getValue("user_id"))
    let sessionId: Int64 = Int64(jsonResponse.getValue("session_id"))
    do {
        if let user = try? User.createUser(id: id, context: PersistenceService.context) {
            if let user = user {
                user.email = jsonResponse.getValue("user_email")
                user.fullname = jsonResponse.getValue("user_fullname")
                user.id = id
                user.username = jsonResponse.getValue("user_name")
                if let session = try? Session.createSession(id: sessionId, context: PersistenceService.context) {
                    if let session = session {
                        session.id = sessionId
                        session.token = jsonResponse.getValue("token")
                        user.session = session
                        session.user = user
                    }
                }
                PFLoginSetStateLogin(user: user)
            }
        }
    }
    PersistenceService.saveContext()
    return (true, "")
}

public var PFLoginExists: Bool  {
    return LoginExists
}

public var PFLoginGetSessionID: String {
    return String(LoginSessionID)
}

public var PFLoginGetSessionToken: String {
    return LoginSessionToken
}

public var PFLoginGetUserFullName: String {
    return LoginUserFullName
}

public var PFLoginGetUserEmail: String {
    return LoginUserEmail
}

public var PFLoginUser: User? {
    return CurrentUser
}
