//
//  PFFavourite.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 29/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//   This module handles Favorite changes and updates
// ------------------------------------------------------------------------------------------------------------


import Foundation

func PFFavoriteListUpdated(courseIDThatHasChangedFavoriteStatus updatedCourseID: Int64, newFavoriteStatus favorited: Bool) {
    PFBackendFetchCourses()
}

func PFFavoriteAdd(courseID: Int64, parseFunction: @escaping (Bool, String) -> ()) {
    // Add Favourite Backend Api CALL
    JSONRequest(operation: "favourite", parameters: ["course_id": String(courseID)]) { (res) in
        let already: Bool = res.getValue("already")
        if (!already) {
            PFEnrollListUpdated(courseIDThatHasChangedEnrollStatus: courseID, newEnrollStatus: true)
        }
        parseFunction(res.success, res.error)
    }
}

func PFFavoriteRemove(courseID: Int64, parseFunction: @escaping (Bool, String) -> ()) {
    // Remove Favourite Backend API Call
    JSONRequest(operation: "favourite", parameters: ["course_id": String(courseID), "remove": "true"]) { (res) in
        parseFunction(res.success, res.error)
        if (res.success) {
            PFEnrollListUpdated(courseIDThatHasChangedEnrollStatus: courseID, newEnrollStatus: false)
        }
    }
}
