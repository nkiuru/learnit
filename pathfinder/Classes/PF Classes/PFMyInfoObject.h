//
//  PFMyInfoObject.h
//  pathfinder
//
//  Created by Samuli Virtanen on 02/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// PFMyInfoObject Class implemented in Objective-C just for fun and test purposes (how objC classes can be used in a Swift project)!

#ifndef PFMyInfoObject_h
#define PFMyInfoObject_h

#import <Foundation/Foundation.h>

@interface PFMyInfoObject : NSObject

@property int courseCountEnrolled;
@property int courseCountFinished;
@property int courseECTSEnrolled;
@property int courseECTSGathered;


@end

#endif /* PFMyInfoObject_h */
