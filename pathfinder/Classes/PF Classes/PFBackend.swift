//
//  PFBackend.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//   This module contains all the basic backend data fetch function calls.
// ------------------------------------------------------------------------------------------------------------

import Foundation

// Fetch all from Backend. Please use only when all the data must be fetched (eg when changing language)
func PFBackendFetchAll() {
    print("[PFMain:PFMainBackendFetchAll]")
    PFBackendFetchLoginEssential()
}

// Use this API call when inlogging
func PFBackendFetchLoginEssential() {
    print("[PFMain:PFMainBackendFetchLoginEssential]")
    
    // Determine System Language
    let systemLanguageCode = Locale.current.languageCode
    let language : PFLanguage = (systemLanguageCode ?? "") == "fi" ? .FI : .EN
    print("[PFBackend:PFBackendFetchLoginEssential] Current Local Language: \(language)")
    
    // Set Language to Backend
    PFBackendSetLanguage(language: language)
    
    PFBackendFetchProviders()
    PFBackendFetchCategories()
    PFBackendFetchCourses()
    PFBackendFetchCourseCategoryLinkings()
    PFBackendFetchLanguages()
    
    ALGOStudyPathsLoadIntoCoreData() // Load StudyPaths
}

// Fetch Backend Languages
func PFBackendFetchLanguages() {
    APIData(load: .Languages) { (success, error) in
        print("[PF:PFBackendFetchLanguages] APIData[load: Languages] Success=\(success) \(error)")
    }
}

// Fetch Course Category Linkings from Backend
func PFBackendFetchCourseCategoryLinkings() {
    APIData(load: .CourseCategoryLinkings) { (success, error) in
        print("[PF:PFBackendFetchCourseCategoryLinkings] APIData[load: CourseCategoryLinkings] Success=\(success) \(error)")
    }
}

// Fetch Course list from Backend
func PFBackendFetchCourses() {
    APIData(load: .Courses) { (success, error) in
        print("[PF:PFBackendFetchCourses] APIData[load: Courses] Success=\(success) \(error)")
    }
}

// Fetch providers from Backend
func PFBackendFetchProviders() {
    APIData(load: .Providers) { (success, error) in
        print("[PF:PFBackendFetchProviders] APIData[load: Providers] Success=\(success) \(error)")
    }
}

// Fetch Categories from Backend
func PFBackendFetchCategories() {
    APIData(load: .Categories) { (success, error) in
        print("[PF:PFBackendFetchCategories] APIData[load: Categories] Success=\(success) \(error)")
    }
}

// Save Studypath to Backend
func PFBackendSaveStudyPath(newStudyPath: [Int64], response: @escaping (Bool, String) -> ()) {
    if !PFLoginExists {
        response(false, "This operation needs user logged into the system.")
    }
    var studyPathString = ""
    for id in newStudyPath {
        studyPathString += (studyPathString != "" ? "-" : "") + String(id)
    }
    JSONRequest(operation: "save-studypath", parameters: ["path": studyPathString]) { (res) in
        response(res.success, res.error)
    }
}

enum PFLanguage : String {
    case EN = "EN", FI = "FI"
}

func PFBackendSetLanguage(language: PFLanguage) {
    let langCode = language.rawValue
    JSONRequest(operation: "set-language", parameters: ["language": langCode]) { (response) in
        if !response.success {
            print("[PFBackend:PFBackendSetLanguage] Backend language Set Failed. Error: \(response.error)")
        }
    }
}
