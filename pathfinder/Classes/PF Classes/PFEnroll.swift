//
//  PFEnroll.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//   Enroll / UnEnroll JSONRequests.
// ------------------------------------------------------------------------------------------------------------

import Foundation

func PFEnrollListUpdated(courseIDThatHasChangedEnrollStatus updatedCourseID: Int64, newEnrollStatus enrolled: Bool) {
    // This function is called, when core data needs updating (new enrollment)
    //print("[PFEnroll] TODO: ENROLLED LIST HAS CHANGED! COREDATA MUST BE UPDATED *TODO*")
    PFBackendFetchCourses()
}

func PFEnrollToCourse(courseID: Int64, parseFunction: @escaping (Bool, String) -> ()) {
    // Enroll to Course Backend API call
    print("[PFEnrollToCourse] CourseID:\(courseID)")
    JSONRequest(operation: "enroll", parameters: ["course_id": String(courseID)]) { (res) in
        let already: Bool = res.getValue("already")
        if (!already) {
            
        }
        PFEnrollListUpdated(courseIDThatHasChangedEnrollStatus: courseID, newEnrollStatus: true)
        parseFunction(res.success, res.error)
    }
}

func PFEnrollToCourseCancelEnrollment(courseID: Int64, parseFunction: @escaping (Bool, String) -> ()) {
    // Enroll to Course Backend API call
    JSONRequest(operation: "enroll", parameters: ["course_id": String(courseID), "remove": "true"]) { (res) in
        parseFunction(res.success, res.error)
        if (res.success) {
            PFEnrollListUpdated(courseIDThatHasChangedEnrollStatus: courseID, newEnrollStatus: false)
        }
    }
}
