//
//  PFStudypath.swift
//  pathfinder
//
//  Created by iosdev on 06/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//   This module has functions for Study path data fetching from Core Data
// ------------------------------------------------------------------------------------------------------------


import Foundation
import CoreData

// Get Courses, takes an csv string which contains IDs of courses
func PFFetchStudypathCourses(courses: String) -> [Courses]{
    let courseIds = courses.split(separator: ",")
    var courses: [Courses] = []
    courseIds.forEach({id in
        let trimmedId = id.trimmingCharacters(in: .whitespaces)
        if let course = getCourse(id: Int64(trimmedId) ?? 0) {
            courses.append(course)
        }
    })
    return courses
}

func PFFetchStudypath() -> [Courses]?{
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    courseRequest.predicate = NSPredicate(format: "studypath == %d", 1)
    if let courses = try? PersistenceService.context.fetch(courseRequest) {
        return courses
    }
    return nil
    
}

// Stupid way of getting the main category of user's study path
func PFFetchStudypathCategory() -> Int64? {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    courseRequest.predicate = NSPredicate(format: "studypath == %d", 1)
    if let courses = try? PersistenceService.context.fetch(courseRequest) {
        if courses.count > 0 {
            let categoryIds = courses[0].categories?.split(separator: ",")
            for id in categoryIds ?? [] {
                if let mainCategory = PFCategoriesFetchById(id: Int64(id) ?? 0) {
                    if mainCategory.parent == 0 {
                        return mainCategory.id
                    }
                }
            }
        }
    }
    return nil
}
