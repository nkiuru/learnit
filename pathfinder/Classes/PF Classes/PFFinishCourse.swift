//
//  PFFinishCourse.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 01/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// ------------------------------------------------------------------------------------------------------------
//   Finish Course Backend Function calls and API communication is found here.
// ------------------------------------------------------------------------------------------------------------

import Foundation

func PFFinishedCourseListUpdated(courseIDThatHasChangedFinishStatus updatedCourseID: Int64, newFinishStatus enrolled: Bool) {
    PFBackendFetchCourses()
}

func PFFinishCourse(courseID: Int64, parseFunction: @escaping (Bool, String) -> ()) {
    // Finish Course Backend API call
    JSONRequest(operation: "finish-course", parameters: ["course_id": String(courseID)]) { (res) in
        let already: Bool = res.getValue("already")
        if (!already) {
            PFFinishedCourseListUpdated(courseIDThatHasChangedFinishStatus: courseID, newFinishStatus: true)
        }
        parseFunction(res.success, res.error)
    }
}

func PFFinishCourseReturnToUnfinished(courseID: Int64, parseFunction: @escaping (Bool, String) -> ()) {
    // Finish-->Unfinished Course Backend API call
    JSONRequest(operation: "finish-course", parameters: ["course_id": String(courseID), "remove": "true"]) { (res) in
        if (res.success) {
            PFFinishedCourseListUpdated(courseIDThatHasChangedFinishStatus: courseID, newFinishStatus: false)
        }
        parseFunction(res.success, res.error)
    }
}

