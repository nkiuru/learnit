//
//  PFCategories.swift
//  pathfinder
//
//  Created by Niklas Kiuru on 04/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
// ------------------------------------------------------------------------------------------------------------
//   This module has functions for fetching category information
// ------------------------------------------------------------------------------------------------------------
import Foundation
import CoreData

// Fetches all categories from core data
func PFCategoriesFetchAll() -> [Categories]? {
    let categoriesRequest: NSFetchRequest<Categories> = Categories.fetchRequest()
    
    if let categories = try? PersistenceService.context.fetch(categoriesRequest) {
        return categories
    }
    return nil
}

// Fetches all categories without parent category
func PFCategoriesFetchMain() -> [Categories]? {
    let categoriesRequest: NSFetchRequest<Categories> = Categories.fetchRequest()
    categoriesRequest.predicate = NSPredicate(format: "parent = %d", 0)
    if let categories = try? PersistenceService.context.fetch(categoriesRequest) {
        return categories
    }
    return nil
}

// Fetches all child categories of given parent category
func PFCategoriesFetchChildren(id: Int64) -> [Categories]? {
    let categoriesRequest: NSFetchRequest<Categories> = Categories.fetchRequest()
    categoriesRequest.predicate = NSPredicate(format: "parent = %d", id)
    if let categories = try? PersistenceService.context.fetch(categoriesRequest) {
        return categories
    }
    return nil
}

// Fetches category by id
func PFCategoriesFetchById(id: Int64) -> Categories? {
    let categoriesRequest: NSFetchRequest<Categories> = Categories.fetchRequest()
    categoriesRequest.predicate = NSPredicate(format: "id = %d", id)
    if let categories = try? PersistenceService.context.fetch(categoriesRequest) {
        if categories.count > 0 {
            return categories[0]
        } else if (categories.count > 0) {
            print("[PFCategories:PFCategoriesFetchById] Duplicates in core data")
        } else {
            return nil
        }
    }
    return nil
}

// Fetches all study paths according to skill level
func PFCategoriesFetchStudypaths(skillLevel: Int) -> [StudyPaths]?{
    let pathsRequest: NSFetchRequest<StudyPaths> = StudyPaths.fetchRequest()
    pathsRequest.predicate = NSPredicate(format: "level = %d", skillLevel)
    if let paths = try? PersistenceService.context.fetch(pathsRequest) {
        print(paths)
        return paths
    }
    return nil
}

// Fetches all popular categories
func PFCategoriesFetchPopular() -> [Categories]? {
    let categoriesRequest: NSFetchRequest<Categories> = Categories.fetchRequest()
    categoriesRequest.predicate = NSPredicate(format: "popular = %d", 1)
    if let categories = try? PersistenceService.context.fetch(categoriesRequest) {
        return categories
    }
    return nil
}

// Fetches 5 predetermined subcategories of each main category
// Used to determine best studypath for user
func PFCategoriesFetchOnboarding() -> [Categories?]{
    var onboardingSubcategories: [Categories?] = []
    
    // Programming
    onboardingSubcategories.append(PFCategoriesFetchById(id: 7)) // C++
    onboardingSubcategories.append(PFCategoriesFetchById(id: 18)) // Java
    onboardingSubcategories.append(PFCategoriesFetchById(id: 19)) // Javascript
    onboardingSubcategories.append(PFCategoriesFetchById(id: 112)) // OOP
    onboardingSubcategories.append(PFCategoriesFetchById(id: 28)) // Python
    
    // Design
    onboardingSubcategories.append(PFCategoriesFetchById(id: 40)) // CSS
    onboardingSubcategories.append(PFCategoriesFetchById(id: 41)) // SCSS
    onboardingSubcategories.append(PFCategoriesFetchById(id: 42)) // UI/UX Design
    onboardingSubcategories.append(PFCategoriesFetchById(id: 114)) // Web Design
    onboardingSubcategories.append(PFCategoriesFetchById(id: 115)) // Typography
    
    // Databases
    onboardingSubcategories.append(PFCategoriesFetchById(id: 44)) // MySQL
    onboardingSubcategories.append(PFCategoriesFetchById(id: 46)) // MariaDB
    onboardingSubcategories.append(PFCategoriesFetchById(id: 50)) // Microsoft SQL Server
    onboardingSubcategories.append(PFCategoriesFetchById(id: 54)) // PostgreSQL
    onboardingSubcategories.append(PFCategoriesFetchById(id: 58)) // mongoDB
    
    // Software
    onboardingSubcategories.append(PFCategoriesFetchById(id: 60)) // SAP
    onboardingSubcategories.append(PFCategoriesFetchById(id: 64)) // Microsoft Office
    onboardingSubcategories.append(PFCategoriesFetchById(id: 74)) // Photoshop
    onboardingSubcategories.append(PFCategoriesFetchById(id: 107)) // Winblow$
    onboardingSubcategories.append(PFCategoriesFetchById(id: 109)) // Linux
    
    // Mobile Development
    onboardingSubcategories.append(PFCategoriesFetchById(id: 80)) // iOS Dev
    onboardingSubcategories.append(PFCategoriesFetchById(id: 81)) // Android Development
    onboardingSubcategories.append(PFCategoriesFetchById(id: 82)) // React Native
    onboardingSubcategories.append(PFCategoriesFetchById(id: 83)) // Ionic
    onboardingSubcategories.append(PFCategoriesFetchById(id: 84)) // Flutter
    
    // Machine Learning
    onboardingSubcategories.append(PFCategoriesFetchById(id: 86)) // Artificial neural networks
    onboardingSubcategories.append(PFCategoriesFetchById(id: 87)) // Artificial intelligence
    onboardingSubcategories.append(PFCategoriesFetchById(id: 116)) // TensorFlow
    onboardingSubcategories.append(PFCategoriesFetchById(id: 117)) // Deep learning
    onboardingSubcategories.append(PFCategoriesFetchById(id: 118)) // Speech Recognition
    
    // IOT
    onboardingSubcategories.append(PFCategoriesFetchById(id: 89)) // Arduino
    onboardingSubcategories.append(PFCategoriesFetchById(id: 90)) // Microprocessors
    onboardingSubcategories.append(PFCategoriesFetchById(id: 91)) // Integrated Circuits
    onboardingSubcategories.append(PFCategoriesFetchById(id: 119)) // Automation
    onboardingSubcategories.append(PFCategoriesFetchById(id: 120)) // Embedded Systems
    
    // Data Analytics
    onboardingSubcategories.append(PFCategoriesFetchById(id: 93)) // Big Data
    onboardingSubcategories.append(PFCategoriesFetchById(id: 121)) // Statistics
    onboardingSubcategories.append(PFCategoriesFetchById(id: 122)) // Data Modelling
    onboardingSubcategories.append(PFCategoriesFetchById(id: 123)) // Apache Hadoop
    onboardingSubcategories.append(PFCategoriesFetchById(id: 124)) // Data Processing
    
    // Web dev
    onboardingSubcategories.append(PFCategoriesFetchById(id: 106)) // Node.js
    onboardingSubcategories.append(PFCategoriesFetchById(id: 96)) // Full stack
    onboardingSubcategories.append(PFCategoriesFetchById(id: 98)) // Angular
    onboardingSubcategories.append(PFCategoriesFetchById(id: 99)) // React
    onboardingSubcategories.append(PFCategoriesFetchById(id: 100)) // HTML
    
    // Security & safety
    onboardingSubcategories.append(PFCategoriesFetchById(id: 103)) // Information security
    onboardingSubcategories.append(PFCategoriesFetchById(id: 104)) // Vulnerabilities
    onboardingSubcategories.append(PFCategoriesFetchById(id: 125)) // Encryption
    onboardingSubcategories.append(PFCategoriesFetchById(id: 126)) // Hacking
    onboardingSubcategories.append(PFCategoriesFetchById(id: 127)) // Security Testing
    
    return onboardingSubcategories
}


