//
//  PFMyInfoObject.m
//  pathfinder
//
//  Created by Samuli Virtanen on 02/05/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

// PFMyInfoObject Class implemented in Objective-C just for fun and test purposes (how objC classes can be used in a Swift project)!

#import <Foundation/Foundation.h>
#import "PFMyInfoObject.h"

@implementation PFMyInfoObject

@synthesize courseCountEnrolled;
@synthesize courseCountFinished;
@synthesize courseECTSEnrolled;
@synthesize courseECTSGathered;

@end
