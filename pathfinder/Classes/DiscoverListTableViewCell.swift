//
//  DiscoverListTableViewCell.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 25/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

import UIKit

class DiscoverListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var ects: UILabel!
    @IBOutlet weak var descr: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
