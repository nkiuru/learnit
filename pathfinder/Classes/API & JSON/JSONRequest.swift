//
//  JSONRequest.swift
//  SuperSquadAPIModule
//
//  Created by Samuli Virtanen on 22/04/2019.
//  Copyright © 2019 samuliv. All rights reserved.
/*
 
        _______ ____  _   ______                             __
       / / ___// __ \/ | / / __ \___  ____ ___  _____  _____/ /_
  __  / /\__ \/ / / /  |/ / /_/ / _ \/ __ `/ / / / _ \/ ___/ __/
 / /_/ /___/ / /_/ / /|  / _, _/  __/ /_/ / /_/ /  __(__  ) /_
 \____//____/\____/_/ |_/_/ |_|\___/\__, /\__,_/\___/____/\__/  .swift
                                      /_/
 */

// ------------------------------------------------------------------------------------------------------------
//   This module performs the Backend API JSON Requests (response is always in JSON format), parses the
//   response automatically for quick use format.
//
//   So.... This class/module is like a simple/custom "Alamofire".
//
//   See tests for example use:
//   pathfinderJSONRequestTests.swift
// ------------------------------------------------------------------------------------------------------------

import Foundation

// Public types for JSONRequest module
public enum JSONRequestType {
    case POST, GET
}

// JSONDataObject for storing multiple values and reading the values by Parameter Name in desired format.
public class JSONDataObject {
    var dataValues = [String: String]()
    
    // Add new value (JSON parameter)
    func addValue(name: String, value: String) {
        dataValues[name] = value
    }
    
    // Get value as NSDecimalNumber
    func getValue(_ name: String) -> NSDecimalNumber {
        if let value = dataValues[name] {
            if let valueDec = Decimal(string: value)  {
                return valueDec as NSDecimalNumber
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    // Get value as Int64
    func getValue(_ name: String) -> Int64 {
        if let value = dataValues[name] {
            if let valueInt64 = Int64(value) {
                return valueInt64
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    // Get value as Int
    func getValue(_ name: String) -> Int {
        if let value = dataValues[name] {
            if let valueInt = Int(value) {
                return valueInt
            } else {
                return 0
            }
        } else {
            return 0
        }
    }
    
    // Get value as String
    func getValue(_ name: String) -> String {
        if let value = dataValues[name] {
            return value
        } else {
            return ""
        }
    }
}

// JSONRequestResponse for storing all the JSON Response data and reading the data in easy way and in desired format.
public class JSONRequestResponse {
    var responseDataValues = [String: String]() // Array for storing JSON repsonse items/values
    var responseDataArray = [JSONDataObject]() // Array for storing data list values
    public var success = false
    public var error = ""
    
    init() {
        
    }
    
    /* quick initializer for error response */
    init(error: String) {
        self.error = error
        self.success = false
        responseDataValues["error"] = error
        responseDataValues["success"] = "false"
    }
    
    // Get Data Array Row Item as a NSDecimalNumber
    func getDataItem(row: Int, item: String) -> NSDecimalNumber {
        if let decValue = Decimal(string: responseDataArray[row].getValue(item)) as NSDecimalNumber? {
            return decValue
        }
        return 0.00
    }
    
    // Get Data Array Row Item as a String
    func getDataItem(row: Int, item: String) -> String {
        return responseDataArray[row].getValue(item)
    }
    
    // Get Data Array Row Item as a Int64
    func getDataItem(row: Int, item: String) -> Int64 {
        return Int64(truncating: responseDataArray[row].getValue(item))
    }
    
    // Get Data Array Row Item as a NSDate
    func getDataItem(row: Int, item: String) -> NSDate {
        let dateString: String = responseDataArray[row].getValue(item)
        if dateString == "" {
            return NSDate()
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
            guard let date = dateFormatter.date(from: dateString) else {
                print("[JSONRequest:getDataItem()->NSDate] Date Format Conversion failed for String: \(dateString)")
                return NSDate()
            }
            return date as NSDate
        }
    }
    
    // Add Data Array Item
    func addDataArrayItem(_ object: JSONDataObject) {
        responseDataArray.append(object)
    }
    
    // Add Item/Value
    func addValue(key: String, value: String) {
        self.responseDataValues[key] = value
        if key == "success" && value == "1" {
            self.success = true
        }
        if key == "error" {
            self.error = value
        }
    }
    
    // Get DataArray Length (length of JSON Reponses "data" -array)
    func dataLength() -> Int {
        return responseDataArray.count
    }
    
    // Get JSON Response item as a String
    func getValue(_ value: String) -> String {
        if let _ = responseDataValues[value] {
            return responseDataValues[value] ?? ""
        } else {
            return ""
        }
    }
    
    // Get JSON Response item as a Int
    func getValue(_ value: String) -> Int {
        if let intVal = Int(responseDataValues[value] ?? "") {
            return intVal
        } else {
            return 0
        }
    }
    
    // Get JSON Response item as a Bool
    func getValue(_ value: String) -> Bool {
        if let boolVal = Bool(responseDataValues[value] ?? "") {
            return boolVal
        } else if let stringVal = responseDataValues[value] {
            if ( stringVal == "true" || stringVal == "1" ) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
}

// Perform a JSONRequest (HTML POST-URLRequest) to BackEnd.
// operation = Backend's operation name, parameters = URLRequest Body Parameters, parseFunction = callback function when req is finished.
public func JSONRequest(operation: String, requestMethod: JSONRequestType, parameters: [String: String]?, setCustomTimeout: Int, parseFunction: @escaping (JSONRequestResponse) -> ()) {
    
    let jsonResponse = JSONRequestResponse()
    
    print("[JSONRequest][\(operation)] NEW JSONRequest STARTED --> \(operation)")
    
    guard let url = URL(string: "https://tucloud.fi/metropolia/supersquad/?operation=" + operation) else {
        fatalError("Failed to create URL")
    }
    
    var request = URLRequest(url: url)

    if requestMethod == .GET {
        request.httpMethod = "GET"
    } else {
        request.httpMethod = "POST"
    }
    request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
    
    request.timeoutInterval = Double(setCustomTimeout)
    
    var params: [String: String] = [:]
    
    if let parametersSet = parameters {
        params = parametersSet
    }

    // Check, is the user logged into the System.
    if PFLoginExists {
        // Add session parameters to the request body part:
        params["session_id"] = PFLoginGetSessionID
        params["token"] = PFLoginGetSessionToken
    }
    
    // If there are some parameters, request.httpBody is set:
    if (params.count > 0) {
        var dataString = ""
        print("[JSONRequest][\(operation)] Parameters:")
        
        // URLEncode all the parameters for httpBody data.
        for (key, val) in params {
            dataString += (dataString != "" ? "&" : "") + (key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "") + "=" + (val.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")
            print("[JSONRequest][\(operation)] \(key)=\(val)")
        }
        
        if let postData : Data = dataString.data(using: .utf8) {
            request.httpBody = postData
        } else {
            print("[JSONRequest][\(operation)] ERROR: Data conversion failed.")
        }
        
    }
    
    // Perform the URLSession dataTask (request) to backend API
    let task = URLSession.shared.dataTask(with: request) { data, response, error in
        
        if let error = error {
            parseFunction(JSONRequestResponse(error: "Network Error / Client error"))
            print("[JSONRequest][\(operation)] Client error: \(error)")
            return
        }
        
        guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
            parseFunction(JSONRequestResponse(error: "Network Error / Server error"))
            print("[JSONRequest][\(operation)] Server Error.")
            return
        }
        
        if let data = data, let _ = String(data: data, encoding: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
                
                if let dataItems = json?["data"] as? [[String: Any]] {
                    for (item) in dataItems {
                        let subitems = item
                        let newDataObj = JSONDataObject()
                        for (key, val) in subitems {
                            if let intValue = val as? Int64 {
                                newDataObj.addValue(name: key, value: String(intValue))
                            } else if let stringValue = val as? String {
                                newDataObj.addValue(name: key, value: stringValue)
                            } else {
                                print("[JSONRequest][\(operation)] Couldn't convert value of key \(key)")
                            }
                        }
                        jsonResponse.addDataArrayItem(newDataObj)
                    }
                } else {
                    print("[JSONRequest] NOTICE: No data array in JSON response.")
                }

                if let json = json {
                    for (key, _) in json {
                        if let intValue = json[key] as? Int {
                            jsonResponse.addValue(key: key, value: String(intValue))
                        } else if let stringValue = json[key] as? String {
                            jsonResponse.addValue(key: key, value: stringValue)
                        } else {
                            if ((key) != "data") {
                                print("[JSONRequest][\(operation)] AUTOPARSE WARNING: Key value `\(key)` couldn't be converted.")
                            }
                        }
                    }
                } else {
                    print("[JSONRequest][\(operation)] ERROR: Couldn't parse JSON data.")
                }
                
            } catch let parsingError {
                print("[JSONRequest][\(operation)] ERROR: Parsing Error \(parsingError)")
            }
            
            // Call the callback function.
            parseFunction(jsonResponse)

            print("[JSONRequest][\(operation)] JSONRequest \(operation) --> FINISHED.")
        }
    }
    task.resume()
}

// JSONRequest without Request Method Type (will be using POST) & Timeout
public func JSONRequest(operation: String, parameters: [String: String]?, parseFunction: @escaping (JSONRequestResponse) -> ()) {
    JSONRequest(operation: operation, requestMethod: .POST, parameters: parameters, setCustomTimeout: 55, parseFunction: parseFunction)
}

// JSONRequest without Request Method Type (will be using POST)
public func JSONRequest(operation: String, parameters: [String: String]?, setCustomTimeout: Int, parseFunction: @escaping (JSONRequestResponse) -> ()) {
    JSONRequest(operation: operation, requestMethod: .POST, parameters: parameters, setCustomTimeout: setCustomTimeout, parseFunction: parseFunction)
}
