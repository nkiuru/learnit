//
//  APIDataModule.swift
//  pathfinder
//
//  Created by Samuli Virtanen on 25/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//

/*
      ___    ____  ________        __
    /   |  / __ \/  _/ __ \____ _/ /_____ _
   / /| | / /_/ // // / / / __ `/ __/ __ `/
  / ___ |/ ____// // /_/ / /_/ / /_/ /_/ /
 /_/  |_/_/   /___/_____/\__,_/\__/\__,_/  .swift
 
 */


// ------------------------------------------------------------------------------------------------------------
//   This module:
//     1) performs Backend API Data URLRequest to the backend using JSONRequest
//     2) converts API JSON response Data "APIData"
//     3) stores/updates CoreData.
// ------------------------------------------------------------------------------------------------------------

import Foundation
import CoreData

// Backend API defined operations:
public enum APIDataType {
    case Courses, Languages, Categories, Providers, CourseCategoryLinkings
}

// Load defined APIDataType data from backend
public func APIData(load: APIDataType, _ responseSuccessAndErrorText: @escaping (Bool, String) -> ()) {
    var operation = "unknown"
    
    // Describe backend operation names for the APIDataType enum.
    switch load {
    case .Categories:
        operation = "categories"
    case .Courses:
        operation = "courses"
    case .Languages:
        operation = "languages"
    case .Providers:
        operation = "providers"
    case .CourseCategoryLinkings:
        operation = "category-linkings"
    }
    
    // Fetch the data from the Backend API
    JSONRequest(operation: operation, parameters: nil) { (res) in
        if res.success {
            let cdStoreResponse = CDStore(load, res)
            responseSuccessAndErrorText(cdStoreResponse.0, cdStoreResponse.1)
        } else {
            // JSON request failed.
            responseSuccessAndErrorText(false, res.error)
        }
    }
}

// Store data to CoreData function handler.
private func CDStore(_ loadedApiDataType: APIDataType, _ jsonRequestResponse: JSONRequestResponse) -> (Bool, String) {
    
    switch loadedApiDataType {
        
    case .Courses:
        return CDStoreCourses(jsonResponse: jsonRequestResponse)
    case .Languages:
        return CDStoreLanguages(jsonResponse: jsonRequestResponse)
    case .Categories:
        return CDStoreCategories(jsonResponse: jsonRequestResponse)
    case .Providers:
        return CDStoreProviders(jsonResponse: jsonRequestResponse)
    case .CourseCategoryLinkings:
        return CDStoreCourseCategoryLinkings(jsonResponse: jsonRequestResponse)
    }
    
}

// Store Courses to CoreData
private func CDStoreCourses(jsonResponse: JSONRequestResponse) -> (Bool, String) {
    if jsonResponse.dataLength() > 0 {
        for row in 0...jsonResponse.dataLength()-1 {
            let id: Int64 = jsonResponse.getDataItem(row: row, item: "id")
            PersistenceService.context.performAndWait {
                do {
                    if let course = try? Courses.createCourse(id: id, context: PersistenceService.context) {
                        if let courseItem = course {
                            courseItem.descr = jsonResponse.getDataItem(row: row, item: "description")
                            courseItem.duration = jsonResponse.getDataItem(row: row, item: "duration")
                            courseItem.ects = jsonResponse.getDataItem(row: row, item: "ects")
                            courseItem.id = jsonResponse.getDataItem(row: row, item: "id")
                            courseItem.language = jsonResponse.getDataItem(row: row, item: "language")
                            courseItem.level = jsonResponse.getDataItem(row: row, item: "level")
                            courseItem.price = jsonResponse.getDataItem(row: row, item: "price")
                            courseItem.providerCourseID = jsonResponse.getDataItem(row: row, item: "provider_course_id")
                            courseItem.providerID = jsonResponse.getDataItem(row: row, item: "provider_id")
                            courseItem.title = jsonResponse.getDataItem(row: row, item: "title")
                            courseItem.www = jsonResponse.getDataItem(row: row, item: "www")
                            courseItem.finished = jsonResponse.getDataItem(row: row, item: "finished")
                            courseItem.enrolled = jsonResponse.getDataItem(row: row, item: "enrolled")
                            courseItem.favourited = jsonResponse.getDataItem(row: row, item: "favourited")
                            courseItem.categories = jsonResponse.getDataItem(row: row, item: "categories")
                            courseItem.studypath = jsonResponse.getDataItem(row: row, item: "studypath")
                            courseItem.enroll_time = jsonResponse.getDataItem(row: row, item: "enroll_time")
                            courseItem.finish_time = jsonResponse.getDataItem(row: row, item: "finish_time")
                            courseItem.favourite_time = jsonResponse.getDataItem(row: row, item: "favourite_time")
                        }
                    }
                }
            }
        }
        PersistenceService.saveContext()
    }
    return (true, "")
}

// Store CourseCategoryLinkings to CoreData
private func CDStoreCourseCategoryLinkings(jsonResponse: JSONRequestResponse) -> (Bool, String) {
    if jsonResponse.dataLength() > 0 {
        for row in 0...jsonResponse.dataLength()-1 {
            let id: Int64 = jsonResponse.getDataItem(row: row, item: "id")
            PersistenceService.context.performAndWait {
                do {
                    if let ccl = try? CourseCategoryLinkings.createLink(id: id, context: PersistenceService.context) {
                        if let ccl = ccl {
                            ccl.category = jsonResponse.getDataItem(row: row, item: "category")
                            ccl.course   = jsonResponse.getDataItem(row: row, item: "course")
                            ccl.id = id
                        }
                    }
                }
            }
        }
        PersistenceService.saveContext()
    }
    return (true, "")
}

// Store Providers to CoreData
private func CDStoreProviders(jsonResponse: JSONRequestResponse) -> (Bool, String) {
    if jsonResponse.dataLength() > 0 {
        for row in 0...jsonResponse.dataLength()-1 {
            let id: Int64 = jsonResponse.getDataItem(row: row, item: "id")
            PersistenceService.context.performAndWait {
            do {
                if let provider = try? Providers.createProvider(id: id, context: PersistenceService.context) {
                    if let provider = provider {
                        provider.name =     jsonResponse.getDataItem(row: row, item: "name")
                        provider.country = jsonResponse.getDataItem(row: row, item: "country")
                        provider.logoURL = jsonResponse.getDataItem(row: row, item: "logo")
                        provider.webpageURL = jsonResponse.getDataItem(row: row, item: "webpage")
                        provider.descr = jsonResponse.getDataItem(row: row, item: "descr")
                        provider.id = id
                    }
                }
            }
            }
        }
        PersistenceService.saveContext()
    }
    return (true, "")
}

// Get Course by id
public func getCourse(id: Int64) -> Courses? {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    courseRequest.predicate = NSPredicate(format: "id = %d", id)
    if let course = try? PersistenceService.context.fetch(courseRequest) {
        print("[APIData:getCourse] Course: \(course)")
        if course.count > 0 {
            return course[0]
        } else if (course.count > 0) {
            print("[APIData:getCourse] Duplicates in core data")
        } else {
            return nil
        }
    }
    return nil
}

// Get Courses
public func getCourses() -> [Courses]? {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    if let courses = try? PersistenceService.context.fetch(courseRequest) {
       return courses
    }
    return nil
}

// Get Provider by id
public func getProvider(id: Int64) -> Providers? {
    let providerRequest: NSFetchRequest<Providers> = Providers.fetchRequest()
    providerRequest.predicate = NSPredicate(format: "id == %d", id)
    if let provider = try? PersistenceService.context.fetch(providerRequest) {
        //print("[APIData:getProvider] Provider: \(provider)")
        if provider.count > 0 {
            return provider[0]
        } else if (provider.count > 0) {
            print("[APIData:getProvider] Duplicates in core data")
        } else {
            return nil
        }
    }
    return nil
}

// Update Course CoreData value
public func updateCourse(id: Int64, key: String, value: Any) -> (Bool, Courses?) {
    let courseRequest: NSFetchRequest<Courses> = Courses.fetchRequest()
    courseRequest.predicate = NSPredicate(format: "id = %d", id)
    if let course = try? PersistenceService.context.fetch(courseRequest) {
        print("[APIData:getCourse] Course: \(course)")
        if course.count > 0 {
            let obj = course[0] as NSManagedObject
            obj.setValue(value, forKey: key)
            PersistenceService.saveContext()
            return (true, course[0])
        } else if (course.count > 0) {
            print("[APIData:getCourse] Duplicates in core data")
        } else {
            return (false,nil)
        }
    }
    return (false, nil)
}

// Store Languages to CoreData
private func CDStoreLanguages(jsonResponse: JSONRequestResponse) -> (Bool, String) {
    if jsonResponse.dataLength() > 0 {
        for row in 0...jsonResponse.dataLength()-1 {
            let id: Int64 = jsonResponse.getDataItem(row: row, item: "id")
            do {
                PersistenceService.context.performAndWait {
                    if let lang = try? Languages.createLanguage(id: id, context: PersistenceService.context) {
                        if let lang = lang {
                            lang.code     = jsonResponse.getDataItem(row: row, item: "code")
                            lang.language = jsonResponse.getDataItem(row: row, item: "language")
                            lang.id = id
                        }
                    }
                }
            }
        }
        PersistenceService.saveContext()
    }
    return (true, "")
}

// Store Categories to CoreData
private func CDStoreCategories(jsonResponse: JSONRequestResponse) -> (Bool, String) {
    if jsonResponse.dataLength() > 0 {
        for row in 0...jsonResponse.dataLength()-1 {
            let id: Int64 = jsonResponse.getDataItem(row: row, item: "id")
            do {
                PersistenceService.context.performAndWait {
                if let cat = try? Categories.createCategory(id: id, context: PersistenceService.context) {
                    if let cat = cat {
                        cat.category   = jsonResponse.getDataItem(row: row, item: "name")
                        cat.level  = jsonResponse.getDataItem(row: row, item: "level")
                        cat.parent = jsonResponse.getDataItem(row: row, item: "parent")
                        cat.popular = jsonResponse.getDataItem(row: row, item: "popular")
                        cat.id = id
                    }
                }
                }
            }
        }
        PersistenceService.saveContext()
    }
    return (true, "")
}
