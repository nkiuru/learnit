//
//  Session+CoreDataClass.swift
//  pathfinder
//
//  Created by iosdev on 29/04/2019.
//  Copyright © 2019 super squab. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Session)
public class Session: NSManagedObject {
    class func createSession(id: Int64, context: NSManagedObjectContext) throws -> Session? {
        let request: NSFetchRequest<Session> = Session.fetchRequest()
        request.predicate = NSPredicate(format: "id == %d", id)
        do {
            let matchingSessions = try context.fetch(request)
            if matchingSessions.count == 1 {
                return nil
            } else if matchingSessions.count == 0 {
                return Session(context: context)
            } else {
                print("Duplicate session")
                return nil
            }
        } catch {
            throw error
        }
    }
}
